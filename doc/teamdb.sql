/*
Navicat MySQL Data Transfer

Source Server         : my-esc-team
Source Server Version : 50717
Source Host           : 120.77.146.74:3306
Source Database       : teamdb

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2017-06-10 12:35:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `name` varchar(100) CHARACTER SET latin1 DEFAULT NULL COMMENT '用户名称',
  `mobilel` varchar(40) CHARACTER SET latin1 DEFAULT NULL COMMENT '手机号码',
  `nickName` varchar(40) CHARACTER SET latin1 DEFAULT NULL COMMENT '昵称',
  `password` varchar(40) CHARACTER SET latin1 DEFAULT NULL COMMENT '密码',
  `lastLoginTime` datetime DEFAULT NULL COMMENT '最后登录时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统_用户表';

-- ----------------------------
-- Table structure for sys_vote
-- ----------------------------
DROP TABLE IF EXISTS `sys_vote`;
CREATE TABLE `sys_vote` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '投票ID',
  `progremId` int(11) DEFAULT NULL COMMENT '项目ID',
  `result` int(11) DEFAULT NULL COMMENT '投票结果1通过2不通过0投票中',
  `voteDesc` varchar(200) DEFAULT NULL COMMENT '投票描述',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `createUserId` int(11) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统_投票表';

-- ----------------------------
-- Table structure for sys_vote_ext
-- ----------------------------
DROP TABLE IF EXISTS `sys_vote_ext`;
CREATE TABLE `sys_vote_ext` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '投票选项ID',
  `voteId` int(11) DEFAULT NULL COMMENT '投票ID',
  `name` varchar(50) DEFAULT NULL COMMENT '投票选项名',
  `value` varchar(50) DEFAULT NULL COMMENT '投票选项值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统_投票选项表';

-- ----------------------------
-- Table structure for sys_vote_result
-- ----------------------------
DROP TABLE IF EXISTS `sys_vote_result`;
CREATE TABLE `sys_vote_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '投票ID',
  `voteId` int(11) DEFAULT NULL COMMENT '投票Id',
  `userId` int(11) DEFAULT NULL COMMENT '投票人员ID',
  `result` int(11) DEFAULT NULL COMMENT '状态1加入2待投票0退出',
  `voteTime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统_投票_结果表';

-- ----------------------------
-- Table structure for team_progrem
-- ----------------------------
DROP TABLE IF EXISTS `team_progrem`;
CREATE TABLE `team_progrem` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '项目ID',
  `name` varchar(32) DEFAULT NULL COMMENT '项目名称',
  `imagePath` varchar(100) DEFAULT NULL COMMENT '图标地址',
  `descript` varchar(1000) DEFAULT NULL COMMENT '项目描述',
  `voteDesc` varchar(1000) DEFAULT NULL COMMENT '惩罚说明',
  `isSignIn` int(11) DEFAULT NULL COMMENT '是否需要签到',
  `lastUpdateUserId` int(11) DEFAULT NULL COMMENT '最后修改人',
  `updateTime` datetime DEFAULT NULL COMMENT '最后修改时间',
  `orderNo` int(11) DEFAULT '0' COMMENT '排序数字',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='项目';

-- ----------------------------
-- Table structure for team_progrem_penalty
-- ----------------------------
DROP TABLE IF EXISTS `team_progrem_penalty`;
CREATE TABLE `team_progrem_penalty` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '惩罚ID',
  `userId` int(11) DEFAULT NULL COMMENT '惩罚人员ID',
  `describ` varchar(1000) DEFAULT NULL COMMENT '惩罚说明',
  `status` int(11) DEFAULT NULL COMMENT '惩罚状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目_惩罚';

-- ----------------------------
-- Table structure for team_progrem_sign
-- ----------------------------
DROP TABLE IF EXISTS `team_progrem_sign`;
CREATE TABLE `team_progrem_sign` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '签到ID',
  `userId` int(11) DEFAULT NULL COMMENT '签到用户ID',
  `progremId` int(11) DEFAULT NULL COMMENT '签到项目',
  `sginTime` datetime DEFAULT NULL COMMENT '签到时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='签到表';

-- ----------------------------
-- Table structure for team_progrem_summ
-- ----------------------------
DROP TABLE IF EXISTS `team_progrem_summ`;
CREATE TABLE `team_progrem_summ` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分类ID',
  `descript` varchar(1000) DEFAULT NULL COMMENT '分类描述',
  `isShow` int(11) DEFAULT NULL COMMENT '是否可见',
  `createId` int(11) DEFAULT NULL COMMENT '总结人',
  `lastUpdateTime` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目总结';

-- ----------------------------
-- Table structure for team_progrem_user
-- ----------------------------
DROP TABLE IF EXISTS `team_progrem_user`;
CREATE TABLE `team_progrem_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '项目人员ID',
  `progremId` int(11) DEFAULT NULL COMMENT '项目ID',
  `userId` int(11) DEFAULT NULL COMMENT '人员ID',
  `joinTime` datetime DEFAULT NULL COMMENT '加入时间',
  `status` int(11) DEFAULT NULL COMMENT '状态1加入2待投票0退出',
  `createTime` datetime DEFAULT NULL COMMENT '申请时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目_人员';
