package cn.gmph.common.constant;

import java.util.Map;
import java.util.TreeMap;

public class Constants {
    // app交易状态转换
    public enum ErrorCodeEnum {
                               SYS_ERROR("9999",
                                         "系统异常"), TWO("",
                                                      "刷卡收入"), THREE("", "新人红包"), FOUR("", "预约提现");

        private String code;
        private String message;

        ErrorCodeEnum(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public Map<String, String> getMap() {
            Map<String, String> map = new TreeMap<>();
            for (ErrorCodeEnum eType : ErrorCodeEnum.values()) {
                map.put(eType.code, eType.message);
            }
            return map;
        }

        public String getDescByCode(String code) {
            for (ErrorCodeEnum eType : ErrorCodeEnum.values()) {
                if (eType.code == code) {
                    return eType.message;
                }
            }
            return "";
        }

        /**
         * @return the code
         */
        public String getCode() {
            return code;
        }

        /**
         * @return the message
         */
        public String getMessage() {
            return message;
        }

    }
}
