package cn.gmph.common.utils;

import java.io.IOException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

public class HttpUtil {
	public static int HTTP_STATUS_CODE = 200;

	/**
	 * Get请求
	 * 
	 * @param url
	 * @return
	 * @throws HttpException
	 */
	public static String httpGet(String url) throws HttpException {
		return httpGet(url, "utf-8");
	}	

	/**
	 * Get请求
	 * 
	 * @param url
	 * @return
	 * @throws HttpException
	 */
	public static String httpGet(String url, String charset)
			throws HttpException {
		String result = "";
		// 创建HttpClientBuilder
		HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
		// HttpClient
		CloseableHttpClient closeableHttpClient = httpClientBuilder.build();
		HttpGet httpGet = new HttpGet(url);
		System.out.println(httpGet.getRequestLine());
		try {
			// 执行get请求
			HttpResponse httpResponse = closeableHttpClient.execute(httpGet);
			// 获取响应消息实体
			HttpEntity entity = httpResponse.getEntity();
			// 响应状态
			int statusCode = httpResponse.getStatusLine().getStatusCode();
			if (HTTP_STATUS_CODE != statusCode) {
				throw new HttpException(String.format(
						"HTTP error.Wrong statusCode:%s.", statusCode));
			}
			// 判断响应实体是否为空
			if (entity != null) {
				// entity.getContentEncoding();
				result = EntityUtils.toString(entity, charset);
			}

			return result;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try { // 关闭流并释放资源
				closeableHttpClient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	public static String httpPost(String url, List<NameValuePair> formparams) {
		String result = "";
		// 创建HttpClientBuilder
		HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
		// HttpClient
		CloseableHttpClient closeableHttpClient = httpClientBuilder.build();

		HttpPost httpPost = new HttpPost(url);
		// httpPost.setConfig(DEFAULT);
		// 创建参数队列
		// List<NameValuePair> formparams = new ArrayList<NameValuePair>();
		// formparams.add(new BasicNameValuePair("searchText", "英语"));

		UrlEncodedFormEntity entity;
		try {
			entity = new UrlEncodedFormEntity(formparams, "UTF-8");
			httpPost.setEntity(entity);

			HttpResponse httpResponse;
			// post请求
			httpResponse = closeableHttpClient.execute(httpPost);

			// getEntity()
			HttpEntity httpEntity = httpResponse.getEntity();
			if (httpEntity != null) {
				// 响应内容
				result = EntityUtils.toString(httpEntity, "UTF-8");
				return result;
			}
			// 释放资源
			closeableHttpClient.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;

	}

}
