package cn.gmph.common.utils;

import org.apache.commons.lang3.StringUtils;

public class NumberUtil {
    public static long toLong(String str) {
        if (StringUtils.isBlank(str) || !StringUtils.isNumeric(str)
            || StringUtils.equalsIgnoreCase(str, "null")) {
            return 0l;
        }
        return org.apache.commons.lang3.math.NumberUtils.toLong(str);
    }

    public static long toLong(Object obj) {
        return toLong(String.valueOf(obj));
    }
    public static int toInt(String str) {
        if (StringUtils.isBlank(str) || !StringUtils.isNumeric(str)
            || StringUtils.equalsIgnoreCase(str, "null")) {
            return 0;
        }
        return org.apache.commons.lang3.math.NumberUtils.toInt(str);
    }

    public static int toInt(Object obj) {
        return toInt(String.valueOf(obj));
    }
}
