package cn.gmph.team.service.sys.dao.helper;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang3.StringUtils;

import cn.gmph.team.service.sys.entity.VoteResultDO;
public class VoteResultProvider {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String TABLE_NAME = "sys_vote_result";

    public String update(Map<String, Object> params) {
        VoteResultDO voteResult = (VoteResultDO) params.get("voteResult");
        BEGIN();
        UPDATE(TABLE_NAME);
        if (voteResult.getVoteId() != null) {
            SET("voteId=#{voteResult.voteId}");
        }
        if (voteResult.getUserId() != null) {
            SET("userId=#{voteResult.userId}");
        }
        if (voteResult.getResult() != null) {
            SET("result=#{voteResult.result}");
        }
        if (voteResult.getVoteTime() != null) {
            SET("voteTime=#{voteResult.voteTime}");
        }
        WHERE("id = #{voteResult.id}");
        String sql = SQL();
        return sql;
    }

    public String pageList(Map<String, Object> params) {
        VoteResultDO voteResult = (VoteResultDO) params.get("voteResult");
        Integer pageNum = (Integer) params.get("pageNum");
        Integer pageSize = (Integer) params.get("pageSize");
        BEGIN();
        SELECT("*");
        FROM(TABLE_NAME);
        if (voteResult.getId() != null) {
            WHERE("id=#{voteResult.id}");
        }
        if (voteResult.getVoteId() != null) {
            WHERE("voteId=#{voteResult.voteId}");
        }
        if (voteResult.getUserId() != null) {
            WHERE("userId=#{voteResult.userId}");
        }
        if (voteResult.getResult() != null) {
            WHERE("result=#{voteResult.result}");
        }
        if (voteResult.getVoteTime() != null) {
            WHERE("voteTime=#{voteResult.voteTime}");
        }
        ORDER_BY("id desc");
        String sql = SQL();
        int start = 0;
        int limit = 0;
        if (pageNum == null || pageNum == 0) {
            pageNum = 1;
        }
        if (pageSize == null || pageSize == 0) {
            pageSize = 20;
        }
        start = (pageNum - 1) * pageSize;
        limit = pageSize;
        sql += " limit " + start + ", " + limit;
        return sql;
    }

    public String pageListCount(Map<String, Object> params) {
        VoteResultDO voteResult = (VoteResultDO) params.get("voteResult");
        BEGIN();
        SELECT("count(1)");
        FROM(TABLE_NAME);
        if (voteResult.getId() != null) {
            WHERE("id=#{voteResult.id}");
        }
        if (voteResult.getVoteId() != null) {
            WHERE("voteId=#{voteResult.voteId}");
        }
        if (voteResult.getUserId() != null) {
            WHERE("userId=#{voteResult.userId}");
        }
        if (voteResult.getResult() != null) {
            WHERE("result=#{voteResult.result}");
        }
        if (voteResult.getVoteTime() != null) {
            WHERE("voteTime=#{voteResult.voteTime}");
        }
        String sql = SQL();
        return sql;
    }
}

