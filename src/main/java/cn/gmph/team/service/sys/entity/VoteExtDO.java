package cn.gmph.team.service.sys.entity;


public class VoteExtDO {

    /**
     * 投票选项ID
     */
    private Integer id;

    /**
     * 投票ID
     */
    private Integer voteId;

    /**
     * 投票选项名
     */
    private String name;

    /**
     * 投票选项值
     */
    private String value;



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVoteId() {
        return voteId;
    }

    public void setVoteId(Integer voteId) {
        this.voteId = voteId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }



    @Override
    public String toString() {
        return "[id="+ id + ", voteId="+ voteId + ", name="+ name + ", value="+ value + "]";
    }
}