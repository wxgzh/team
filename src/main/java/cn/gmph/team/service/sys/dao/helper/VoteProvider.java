package cn.gmph.team.service.sys.dao.helper;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang3.StringUtils;

import cn.gmph.team.service.sys.entity.VoteDO;
public class VoteProvider {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String TABLE_NAME = "sys_vote";

    public String update(Map<String, Object> params) {
        VoteDO vote = (VoteDO) params.get("vote");
        BEGIN();
        UPDATE(TABLE_NAME);
        if (vote.getType() != null) {
            SET("type=#{vote.type}");
        }
        if (vote.getTypeId() != null) {
            SET("typeId=#{vote.typeId}");
        }
        if (vote.getResult() != null) {
            SET("result=#{vote.result}");
        }
        if (StringUtils.isNotBlank(vote.getVoteDesc())){
            SET("voteDesc=#{vote.voteDesc}");
        }
        if (vote.getCreateTime() != null) {
            SET("createTime=#{vote.createTime}");
        }
        if (vote.getCreateUserId() != null) {
            SET("createUserId=#{vote.createUserId}");
        }
        WHERE("id = #{vote.id}");
        String sql = SQL();
        return sql;
    }

    public String pageList(Map<String, Object> params) {
        VoteDO vote = (VoteDO) params.get("vote");
        Integer pageNum = (Integer) params.get("pageNum");
        Integer pageSize = (Integer) params.get("pageSize");
        BEGIN();
        SELECT("*");
        FROM(TABLE_NAME);
        if (vote.getId() != null) {
            WHERE("id=#{vote.id}");
        }
        if (vote.getType() != null) {
            WHERE("type=#{vote.type}");
        }
        if (vote.getTypeId() != null) {
            WHERE("typeId=#{vote.typeId}");
        }
        if (vote.getResult() != null) {
            WHERE("result=#{vote.result}");
        }
        if (StringUtils.isNotBlank(vote.getVoteDesc())){
            WHERE("voteDesc=#{vote.voteDesc}");
        }
        if (vote.getCreateTime() != null) {
            WHERE("createTime=#{vote.createTime}");
        }
        if (vote.getCreateUserId() != null) {
            WHERE("createUserId=#{vote.createUserId}");
        }
        if(vote.getVoteUserId() != null){
            WHERE("id not in (select voteId from sys_vote_result where userId = #{vote.voteUserId})");
        }
        ORDER_BY(" createTime desc");
        String sql = SQL();
        int start = 0;
        int limit = 0;
        if (pageNum == null || pageNum == 0) {
            pageNum = 1;
        }
        if (pageSize == null || pageSize == 0) {
            pageSize = 20;
        }
        start = (pageNum - 1) * pageSize;
        limit = pageSize;
        sql += " limit " + start + ", " + limit;
        return sql;
    }

    public String pageListCount(Map<String, Object> params) {
        VoteDO vote = (VoteDO) params.get("vote");
        BEGIN();
        SELECT("count(1)");
        FROM(TABLE_NAME);
        if (vote.getId() != null) {
            WHERE("id=#{vote.id}");
        }
        if (vote.getType() != null) {
            WHERE("type=#{vote.type}");
        }
        if (vote.getTypeId() != null) {
            WHERE("typeId=#{vote.typeId}");
        }
        if (vote.getResult() != null) {
            WHERE("result=#{vote.result}");
        }
        if (StringUtils.isNotBlank(vote.getVoteDesc())){
            WHERE("voteDesc=#{vote.voteDesc}");
        }
        if (vote.getCreateTime() != null) {
            WHERE("createTime=#{vote.createTime}");
        }
        if (vote.getCreateUserId() != null) {
            WHERE("createUserId=#{vote.createUserId}");
        }
        String sql = SQL();
        return sql;
    }
}

