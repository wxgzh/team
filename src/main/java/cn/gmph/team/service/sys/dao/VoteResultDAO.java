package cn.gmph.team.service.sys.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import cn.gmph.team.service.sys.entity.VoteResultDO;
import cn.gmph.team.service.sys.dao.helper.VoteResultProvider;

import java.util.List;;

public interface VoteResultDAO {

    @Select("SELECT * FROM sys_vote_result WHERE id = #{id}")
    public VoteResultDO getById(@Param("id") int id);

    @Insert("INSERT into sys_vote_result(id,voteId,userId,result,voteTime) VALUES (#{id},#{voteId},#{userId},#{result},#{voteTime})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    public void insert(VoteResultDO voteResult);

    @Delete("DELETE FROM sys_vote_result WHERE id = #{id}")
    public int deleteById(@Param("id") int id);

    @UpdateProvider(type = VoteResultProvider.class, method = "update")
    public int update(@Param("voteResult") VoteResultDO  voteResult);

    @SelectProvider(type = VoteResultProvider.class, method = "pageList")
    public List<VoteResultDO> pageList(@Param("voteResult") VoteResultDO voteResult, @Param("pageNum") Integer pageNum, @Param("pageSize") Integer pageSize);

    @SelectProvider(type = VoteResultProvider.class, method = "pageListCount")
    public Integer pageListCount(@Param("voteResult") VoteResultDO voteResult);

}