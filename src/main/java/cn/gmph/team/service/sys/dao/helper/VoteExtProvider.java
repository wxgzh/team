package cn.gmph.team.service.sys.dao.helper;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang3.StringUtils;

import cn.gmph.team.service.sys.entity.VoteExtDO;
public class VoteExtProvider {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String TABLE_NAME = "sys_vote_ext";

    public String update(Map<String, Object> params) {
        VoteExtDO voteExt = (VoteExtDO) params.get("voteExt");
        BEGIN();
        UPDATE(TABLE_NAME);
        if (voteExt.getVoteId() != null) {
            SET("voteId=#{voteExt.voteId}");
        }
        if (StringUtils.isNotBlank(voteExt.getName())){
            SET("name=#{voteExt.name}");
        }
        if (StringUtils.isNotBlank(voteExt.getValue())){
            SET("value=#{voteExt.value}");
        }
        WHERE("id = #{voteExt.id}");
        String sql = SQL();
        return sql;
    }

    public String pageList(Map<String, Object> params) {
        VoteExtDO voteExt = (VoteExtDO) params.get("voteExt");
        Integer pageNum = (Integer) params.get("pageNum");
        Integer pageSize = (Integer) params.get("pageSize");
        BEGIN();
        SELECT("*");
        FROM(TABLE_NAME);
        if (voteExt.getId() != null) {
            WHERE("id=#{voteExt.id}");
        }
        if (voteExt.getVoteId() != null) {
            WHERE("voteId=#{voteExt.voteId}");
        }
        if (StringUtils.isNotBlank(voteExt.getName())){
            WHERE("name=#{voteExt.name}");
        }
        if (StringUtils.isNotBlank(voteExt.getValue())){
            WHERE("value=#{voteExt.value}");
        }
        ORDER_BY("value ");
        String sql = SQL();
        int start = 0;
        int limit = 0;
        if (pageNum == null || pageNum == 0) {
            pageNum = 1;
        }
        if (pageSize == null || pageSize == 0) {
            pageSize = 20;
        }
        start = (pageNum - 1) * pageSize;
        limit = pageSize;
        sql += " limit " + start + ", " + limit;
        return sql;
    }

    public String pageListCount(Map<String, Object> params) {
        VoteExtDO voteExt = (VoteExtDO) params.get("voteExt");
        BEGIN();
        SELECT("count(1)");
        FROM(TABLE_NAME);
        if (voteExt.getId() != null) {
            WHERE("id=#{voteExt.id}");
        }
        if (voteExt.getVoteId() != null) {
            WHERE("voteId=#{voteExt.voteId}");
        }
        if (StringUtils.isNotBlank(voteExt.getName())){
            WHERE("name=#{voteExt.name}");
        }
        if (StringUtils.isNotBlank(voteExt.getValue())){
            WHERE("value=#{voteExt.value}");
        }
        String sql = SQL();
        return sql;
    }
}

