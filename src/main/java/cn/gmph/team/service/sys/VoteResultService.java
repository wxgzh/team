package cn.gmph.team.service.sys;

import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.gmph.team.service.sys.dao.VoteResultDAO;
import cn.gmph.team.service.sys.entity.VoteResultDO;
import cn.gmph.common.base.BaseService;
import cn.gmph.common.base.ResultDTO;
import cn.gmph.common.base.ResultPageDTO;

@Service
public class VoteResultService extends BaseService {

 private Logger logger = LoggerFactory.getLogger(this.getClass());
 @Autowired
 private VoteResultDAO voteResultDAO;

 // 分页
 public ResultPageDTO<VoteResultDO> page(VoteResultDO voteResult, Integer pageNum, Integer pageSize) {
     logger.info("开始分页查询VoteResultService.page, voteResult=" + voteResult.toString());
     List<VoteResultDO> pageList = this.voteResultDAO.pageList(voteResult, pageNum, pageSize);
     Integer count = this.voteResultDAO.pageListCount(voteResult);
   ResultPageDTO<VoteResultDO> pager =  new ResultPageDTO<VoteResultDO>(count,pageList);
     return pager;
 }

 // 添加
 public VoteResultDO doAdd (VoteResultDO voteResult,int loginUserId) {
     logger.info("开始添加VoteResultService.add,voteResult=" + voteResult.toString());
     this.voteResultDAO.insert(voteResult);
     return voteResult;
 }

 // 修改
 public Integer doUpdate (VoteResultDO voteResult,Integer loginUserId) {
     logger.info("开始修改VoteResultService.update,voteResult=" + voteResult.toString());
     int rows=this.voteResultDAO.update(voteResult);
     return rows;
 }

 // 删除
 public Integer doDelete (VoteResultDO voteResult,Integer loginUserId) {
     logger.info("开始删除VoteResultService.delete,voteResult=" + voteResult.toString());
     int rows=this.voteResultDAO.deleteById(voteResult.getId());
     return rows;
 }

 // 查询
 public VoteResultDO doQueryById (Integer id) {
     VoteResultDO obj = this.voteResultDAO.getById(id);
     return obj;
 }
}