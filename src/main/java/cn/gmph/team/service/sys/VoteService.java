package cn.gmph.team.service.sys;

import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.gmph.team.service.sys.dao.VoteDAO;
import cn.gmph.team.service.sys.entity.VoteDO;
import cn.gmph.common.base.BaseService;
import cn.gmph.common.base.ResultDTO;
import cn.gmph.common.base.ResultPageDTO;

@Service
public class VoteService extends BaseService {

 private Logger logger = LoggerFactory.getLogger(this.getClass());
 @Autowired
 private VoteDAO voteDAO;

 // 分页
 public ResultPageDTO<VoteDO> page(VoteDO vote, Integer pageNum, Integer pageSize) {
     logger.info("开始分页查询VoteService.page, vote=" + vote.toString());
     List<VoteDO> pageList = this.voteDAO.pageList(vote, pageNum, pageSize);
     Integer count = this.voteDAO.pageListCount(vote);
   ResultPageDTO<VoteDO> pager =  new ResultPageDTO<VoteDO>(count,pageList);
     return pager;
 }

 // 添加
 public VoteDO doAdd (VoteDO vote,int loginUserId) {
     logger.info("开始添加VoteService.add,vote=" + vote.toString());
     this.voteDAO.insert(vote);
     return vote;
 }

 // 修改
 public Integer doUpdate (VoteDO vote,Integer loginUserId) {
     logger.info("开始修改VoteService.update,vote=" + vote.toString());
     int rows=this.voteDAO.update(vote);
     return rows;
 }

 // 删除
 public Integer doDelete (VoteDO vote,Integer loginUserId) {
     logger.info("开始删除VoteService.delete,vote=" + vote.toString());
     int rows=this.voteDAO.deleteById(vote.getId());
     return rows;
 }

 // 查询
 public VoteDO doQueryById (Integer id) {
     VoteDO obj = this.voteDAO.getById(id);
     return obj;
 }
}