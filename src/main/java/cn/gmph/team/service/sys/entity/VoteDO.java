package cn.gmph.team.service.sys.entity;

import java.util.Date;
import java.util.List;

public class VoteDO {

    /**
     * 投票ID
     */
    private Integer         id;

    /**
     * 投票类型0申请加入项目1惩罚
     */
    private Integer         type;

    /**
     * 类型ID（项目Id，惩罚id）
     */
    private Integer         typeId;

    /**
     * 投票结果1通过2不通过0投票中
     */
    private Integer         result;

    /**
     * 投票描述
     */
    private String          voteDesc;

    /**
     * 创建时间
     */
    private Date            createTime;

    /**
     * 创建人
     */
    private Integer         createUserId;
    //投票选项列表
    private List<VoteExtDO> voteExtList;
    //投票人
    private Integer          voteUserId;

    /**
     * @return the voteUserId
     */
    public Integer getVoteUserId() {
        return voteUserId;
    }

    /**
     * @param voteUserId the voteUserId to set
     */
    public void setVoteUserId(Integer voteUserId) {
        this.voteUserId = voteUserId;
    }

    /**
     * @return the voteExtList
     */
    public List<VoteExtDO> getVoteExtList() {
        return voteExtList;
    }

    /**
     * @param voteExtList the voteExtList to set
     */
    public void setVoteExtList(List<VoteExtDO> voteExtList) {
        this.voteExtList = voteExtList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public String getVoteDesc() {
        return voteDesc;
    }

    public void setVoteDesc(String voteDesc) {
        this.voteDesc = voteDesc;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Integer createUserId) {
        this.createUserId = createUserId;
    }

    @Override
    public String toString() {
        return "[id=" + id + ", type=" + type + ", typeId=" + typeId + ", result=" + result + ", voteDesc=" + voteDesc + ", createTime=" + createTime + ", createUserId=" + createUserId + "]";
    }
}