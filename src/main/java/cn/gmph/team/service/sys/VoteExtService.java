package cn.gmph.team.service.sys;

import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.gmph.team.service.sys.dao.VoteExtDAO;
import cn.gmph.team.service.sys.entity.VoteExtDO;
import cn.gmph.common.base.BaseService;
import cn.gmph.common.base.ResultDTO;
import cn.gmph.common.base.ResultPageDTO;

@Service
public class VoteExtService extends BaseService {

 private Logger logger = LoggerFactory.getLogger(this.getClass());
 @Autowired
 private VoteExtDAO voteExtDAO;

 // 分页
 public ResultPageDTO<VoteExtDO> page(VoteExtDO voteExt, Integer pageNum, Integer pageSize) {
     logger.info("开始分页查询VoteExtService.page, voteExt=" + voteExt.toString());
     List<VoteExtDO> pageList = this.voteExtDAO.pageList(voteExt, pageNum, pageSize);
     Integer count = this.voteExtDAO.pageListCount(voteExt);
   ResultPageDTO<VoteExtDO> pager =  new ResultPageDTO<VoteExtDO>(count,pageList);
     return pager;
 }

 // 添加
 public VoteExtDO doAdd (VoteExtDO voteExt,int loginUserId) {
     logger.info("开始添加VoteExtService.add,voteExt=" + voteExt.toString());
     this.voteExtDAO.insert(voteExt);
     return voteExt;
 }

 // 修改
 public Integer doUpdate (VoteExtDO voteExt,Integer loginUserId) {
     logger.info("开始修改VoteExtService.update,voteExt=" + voteExt.toString());
     int rows=this.voteExtDAO.update(voteExt);
     return rows;
 }

 // 删除
 public Integer doDelete (VoteExtDO voteExt,Integer loginUserId) {
     logger.info("开始删除VoteExtService.delete,voteExt=" + voteExt.toString());
     int rows=this.voteExtDAO.deleteById(voteExt.getId());
     return rows;
 }

 // 查询
 public VoteExtDO doQueryById (Integer id) {
     VoteExtDO obj = this.voteExtDAO.getById(id);
     return obj;
 }
}