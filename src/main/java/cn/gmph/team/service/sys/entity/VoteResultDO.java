package cn.gmph.team.service.sys.entity;

import java.util.Date;

public class VoteResultDO {

    /**
     * 投票ID
     */
    private Integer id;

    /**
     * 投票Id
     */
    private Integer voteId;

    /**
     * 投票人员ID
     */
    private Integer userId;

    /**
     * 状态1加入2待投票0退出
     */
    private Integer result;

    /**
     * 创建时间
     */
    private Date voteTime;



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVoteId() {
        return voteId;
    }

    public void setVoteId(Integer voteId) {
        this.voteId = voteId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public Date getVoteTime() {
        return voteTime;
    }

    public void setVoteTime(Date voteTime) {
        this.voteTime = voteTime;
    }



    @Override
    public String toString() {
        return "[id="+ id + ", voteId="+ voteId + ", userId="+ userId + ", result="+ result + ", voteTime="+ voteTime + "]";
    }
}