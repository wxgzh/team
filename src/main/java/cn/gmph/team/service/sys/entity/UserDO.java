package cn.gmph.team.service.sys.entity;

import java.util.Date;

public class UserDO {

    /**
     * 用户ID
     */
    private Integer id;

    /**
     * 用户名称
     */
    private String  name;

    /**
     * 手机号码
     */
    private String  mobilel;

    /**
     * 昵称
     */
    private String  nickName;

    /**
     * 密码
     */
    private String  password;

    /**
     * 最后登录时间
     */
    private Date    lastLoginTime;

    //改密码时用，原密码
    private String  oldPassword;

    /**
     * @return the oldPassword
     */
    public String getOldPassword() {
        return oldPassword;
    }

    /**
     * @param oldPassword the oldPassword to set
     */
    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobilel() {
        return mobilel;
    }

    public void setMobilel(String mobilel) {
        this.mobilel = mobilel;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    @Override
    public String toString() {
        return "[id=" + id + ", name=" + name + ", mobilel=" + mobilel + ", nickName=" + nickName + ", password=" + password + ", lastLoginTime=" + lastLoginTime + "]";
    }
}