package cn.gmph.team.service.sys.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import cn.gmph.team.service.sys.entity.VoteExtDO;
import cn.gmph.team.service.sys.dao.helper.VoteExtProvider;

import java.util.List;;

public interface VoteExtDAO {

    @Select("SELECT * FROM sys_vote_ext WHERE id = #{id}")
    public VoteExtDO getById(@Param("id") int id);

    @Insert("INSERT into sys_vote_ext(id,voteId,name,value) VALUES (#{id},#{voteId},#{name},#{value})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    public void insert(VoteExtDO voteExt);

    @Delete("DELETE FROM sys_vote_ext WHERE id = #{id}")
    public int deleteById(@Param("id") int id);

    @UpdateProvider(type = VoteExtProvider.class, method = "update")
    public int update(@Param("voteExt") VoteExtDO  voteExt);

    @SelectProvider(type = VoteExtProvider.class, method = "pageList")
    public List<VoteExtDO> pageList(@Param("voteExt") VoteExtDO voteExt, @Param("pageNum") Integer pageNum, @Param("pageSize") Integer pageSize);

    @SelectProvider(type = VoteExtProvider.class, method = "pageListCount")
    public Integer pageListCount(@Param("voteExt") VoteExtDO voteExt);

}