package cn.gmph.team.service.sys;

import java.util.Date;

public class UserBiz {

    // 用户ID
    private Integer id;

    // 用户名称
    private String  name;

    // 手机号码
    private String  mobilel;

    // 昵称
    private String  nickName;

    // 密码
    private String  password;

    // 最后登录时间
    private Date    lastLoginTime;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the mobilel
     */
    public String getMobilel() {
        return mobilel;
    }

    /**
     * @param mobilel the mobilel to set
     */
    public void setMobilel(String mobilel) {
        this.mobilel = mobilel;
    }

    /**
     * @return the nickName
     */
    public String getNickName() {
        return nickName;
    }

    /**
     * @param nickName the nickName to set
     */
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the lastLoginTime
     */
    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    /**
     * @param lastLoginTime the lastLoginTime to set
     */
    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

}