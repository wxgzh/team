package cn.gmph.team.service.sys.dao.helper;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang3.StringUtils;

import cn.gmph.team.service.sys.entity.UserDO;
public class UserProvider {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String TABLE_NAME = "sys_user";

    public String update(Map<String, Object> params) {
        UserDO user = (UserDO) params.get("user");
        BEGIN();
        UPDATE(TABLE_NAME);
        if (StringUtils.isNotBlank(user.getName())){
            SET("name=#{user.name}");
        }
        if (StringUtils.isNotBlank(user.getMobilel())){
            SET("mobilel=#{user.mobilel}");
        }
        if (StringUtils.isNotBlank(user.getNickName())){
            SET("nickName=#{user.nickName}");
        }
        if (StringUtils.isNotBlank(user.getPassword())){
            SET("password=#{user.password}");
        }
        if (user.getLastLoginTime() != null) {
            SET("lastLoginTime=#{user.lastLoginTime}");
        }
        WHERE("id = #{user.id}");
        String sql = SQL();
        return sql;
    }

    public String pageList(Map<String, Object> params) {
        UserDO user = (UserDO) params.get("user");
        Integer pageNum = (Integer) params.get("pageNum");
        Integer pageSize = (Integer) params.get("pageSize");
        BEGIN();
        SELECT("*");
        FROM(TABLE_NAME);
        if (user.getId() != null) {
            WHERE("id=#{user.id}");
        }
        if (StringUtils.isNotBlank(user.getName())){
            WHERE("name=#{user.name}");
        }
        if (StringUtils.isNotBlank(user.getMobilel())){
            WHERE("mobilel=#{user.mobilel}");
        }
        if (StringUtils.isNotBlank(user.getNickName())){
            WHERE("nickName=#{user.nickName}");
        }
        if (StringUtils.isNotBlank(user.getPassword())){
            WHERE("password=#{user.password}");
        }
        if (user.getLastLoginTime() != null) {
            WHERE("lastLoginTime=#{user.lastLoginTime}");
        }
        ORDER_BY("id desc");
        String sql = SQL();
        int start = 0;
        int limit = 0;
        if (pageNum == null || pageNum == 0) {
            pageNum = 1;
        }
        if (pageSize == null || pageSize == 0) {
            pageSize = 20;
        }
        start = (pageNum - 1) * pageSize;
        limit = pageSize;
        sql += " limit " + start + ", " + limit;
        return sql;
    }

    public String pageListCount(Map<String, Object> params) {
        UserDO user = (UserDO) params.get("user");
        BEGIN();
        SELECT("count(1)");
        FROM(TABLE_NAME);
        if (user.getId() != null) {
            WHERE("id=#{user.id}");
        }
        if (StringUtils.isNotBlank(user.getName())){
            WHERE("name=#{user.name}");
        }
        if (StringUtils.isNotBlank(user.getMobilel())){
            WHERE("mobilel=#{user.mobilel}");
        }
        if (StringUtils.isNotBlank(user.getNickName())){
            WHERE("nickName=#{user.nickName}");
        }
        if (StringUtils.isNotBlank(user.getPassword())){
            WHERE("password=#{user.password}");
        }
        if (user.getLastLoginTime() != null) {
            WHERE("lastLoginTime=#{user.lastLoginTime}");
        }
        String sql = SQL();
        return sql;
    }
}

