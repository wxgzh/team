package cn.gmph.team.service.sys.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import cn.gmph.team.service.sys.entity.VoteDO;
import cn.gmph.team.service.sys.dao.helper.VoteProvider;

import java.util.List;;

public interface VoteDAO {

    @Select("SELECT * FROM sys_vote WHERE id = #{id}")
    public VoteDO getById(@Param("id") int id);

    @Insert("INSERT into sys_vote(id,type,typeId,result,voteDesc,createTime,createUserId) VALUES (#{id},#{type},#{typeId},#{result},#{voteDesc},#{createTime},#{createUserId})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    public void insert(VoteDO vote);

    @Delete("DELETE FROM sys_vote WHERE id = #{id}")
    public int deleteById(@Param("id") int id);

    @UpdateProvider(type = VoteProvider.class, method = "update")
    public int update(@Param("vote") VoteDO  vote);

    @SelectProvider(type = VoteProvider.class, method = "pageList")
    public List<VoteDO> pageList(@Param("vote") VoteDO vote, @Param("pageNum") Integer pageNum, @Param("pageSize") Integer pageSize);

    @SelectProvider(type = VoteProvider.class, method = "pageListCount")
    public Integer pageListCount(@Param("vote") VoteDO vote);

}