package cn.gmph.team.service.progrem.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import cn.gmph.team.service.progrem.entity.ProgremUserDO;
import cn.gmph.team.service.progrem.dao.helper.ProgremUserProvider;

import java.util.List;;

public interface ProgremUserDAO {

    @Select("SELECT * FROM team_progrem_user WHERE id = #{id}")
    public ProgremUserDO getById(@Param("id") int id);

    @Select("SELECT * FROM team_progrem_user WHERE userId = #{userId} and progremId = #{progremId} limit 1")
    public ProgremUserDO getByUserId(@Param("userId") int userId,@Param("progremId") int progremId);
    
    @Insert("INSERT into team_progrem_user(id,progremId,userId,joinTime,status,createTime) VALUES (#{id},#{progremId},#{userId},#{joinTime},#{status},#{createTime})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    public void insert(ProgremUserDO progremUser);

    @Delete("DELETE FROM team_progrem_user WHERE id = #{id}")
    public int deleteById(@Param("id") int id);

    @UpdateProvider(type = ProgremUserProvider.class, method = "update")
    public int update(@Param("progremUser") ProgremUserDO  progremUser);

    @SelectProvider(type = ProgremUserProvider.class, method = "pageList")
    public List<ProgremUserDO> pageList(@Param("progremUser") ProgremUserDO progremUser, @Param("pageNum") Integer pageNum, @Param("pageSize") Integer pageSize);

    @SelectProvider(type = ProgremUserProvider.class, method = "pageListCount")
    public Integer pageListCount(@Param("progremUser") ProgremUserDO progremUser);

}