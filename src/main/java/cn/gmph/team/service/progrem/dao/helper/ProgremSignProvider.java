package cn.gmph.team.service.progrem.dao.helper;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang3.StringUtils;

import cn.gmph.team.service.progrem.entity.ProgremSignDO;
public class ProgremSignProvider {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String TABLE_NAME = "team_progrem_sign";

    public String update(Map<String, Object> params) {
        ProgremSignDO progremSign = (ProgremSignDO) params.get("progremSign");
        BEGIN();
        UPDATE(TABLE_NAME);
        if (progremSign.getUserId() != null) {
            SET("userId=#{progremSign.userId}");
        }
        if (progremSign.getProgremId() != null) {
            SET("progremId=#{progremSign.progremId}");
        }
        if (progremSign.getSignTime() != null) {
            SET("signTime=#{progremSign.signTime}");
        }
        WHERE("id = #{progremSign.id}");
        String sql = SQL();
        return sql;
    }

    public String pageList(Map<String, Object> params) {
        ProgremSignDO progremSign = (ProgremSignDO) params.get("progremSign");
        Integer pageNum = (Integer) params.get("pageNum");
        Integer pageSize = (Integer) params.get("pageSize");
        BEGIN();
        SELECT("*");
        FROM(TABLE_NAME);
        if (progremSign.getId() != null) {
            WHERE("id=#{progremSign.id}");
        }
        if (progremSign.getUserId() != null) {
            WHERE("userId=#{progremSign.userId}");
        }
        if (progremSign.getProgremId() != null) {
            WHERE("progremId=#{progremSign.progremId}");
        }
        if (progremSign.getSignTime() != null) {
            WHERE("signTime=#{progremSign.signTime}");
        }
        if (progremSign.getStartTime() != null) {
            WHERE("signTime >= #{progremSign.startTime}");
        }
        ORDER_BY("id desc");
        String sql = SQL();
        int start = 0;
        int limit = 0;
        if (pageNum == null || pageNum == 0) {
            pageNum = 1;
        }
        if (pageSize == null || pageSize == 0) {
            pageSize = 20;
        }
        start = (pageNum - 1) * pageSize;
        limit = pageSize;
        sql += " limit " + start + ", " + limit;
        return sql;
    }

    public String pageListCount(Map<String, Object> params) {
        ProgremSignDO progremSign = (ProgremSignDO) params.get("progremSign");
        BEGIN();
        SELECT("count(1)");
        FROM(TABLE_NAME);
        if (progremSign.getId() != null) {
            WHERE("id=#{progremSign.id}");
        }
        if (progremSign.getUserId() != null) {
            WHERE("userId=#{progremSign.userId}");
        }
        if (progremSign.getProgremId() != null) {
            WHERE("progremId=#{progremSign.progremId}");
        }
        if (progremSign.getSignTime() != null) {
            WHERE("signTime=#{progremSign.signTime}");
        }
        if (progremSign.getStartTime() != null) {
            WHERE("signTime >= #{progremSign.startTime}");
        }
        String sql = SQL();
        return sql;
    }
}

