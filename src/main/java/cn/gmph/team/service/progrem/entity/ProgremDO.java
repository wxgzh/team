package cn.gmph.team.service.progrem.entity;

import java.util.Date;

public class ProgremDO {

    /**
     * 项目ID
     */
    private Integer id;

    /**
     * 项目名称
     */
    private String name;

    /**
     * 图标地址
     */
    private String imagePath;

    /**
     * 项目描述
     */
    private String descript;

    /**
     * 惩罚说明
     */
    private String voteDesc;

    /**
     * 是否需要签到
     */
    private Integer isSignIn;

    /**
     * 最后修改人
     */
    private Integer lastUpdateUserId;

    /**
     * 最后修改时间
     */
    private Date updateTime;

    /**
     * 排序数字
     */
    private Integer orderNo;

    //是否加入
    private Boolean isJoin;
    //是否打卡
    private Boolean isPunishCard;

    /**
     * @return the isJoin
     */
    public Boolean getIsJoin() {
        return isJoin;
    }

    /**
     * @param isJoin the isJoin to set
     */
    public void setIsJoin(Boolean isJoin) {
        this.isJoin = isJoin;
    }

    /**
     * @return the isPunishCard
     */
    public Boolean getIsPunishCard() {
        return isPunishCard;
    }

    /**
     * @param isPunishCard the isPunishCard to set
     */
    public void setIsPunishCard(Boolean isPunishCard) {
        this.isPunishCard = isPunishCard;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript;
    }

    public String getVoteDesc() {
        return voteDesc;
    }

    public void setVoteDesc(String voteDesc) {
        this.voteDesc = voteDesc;
    }

    public Integer getIsSignIn() {
        return isSignIn;
    }

    public void setIsSignIn(Integer isSignIn) {
        this.isSignIn = isSignIn;
    }

    public Integer getLastUpdateUserId() {
        return lastUpdateUserId;
    }

    public void setLastUpdateUserId(Integer lastUpdateUserId) {
        this.lastUpdateUserId = lastUpdateUserId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(Integer orderNo) {
        this.orderNo = orderNo;
    }



    @Override
    public String toString() {
        return "[id="+ id + ", name="+ name + ", imagePath="+ imagePath + ", descript="+ descript + ", voteDesc="+ voteDesc + ", isSignIn="+ isSignIn + ", lastUpdateUserId="+ lastUpdateUserId + ", updateTime="+ updateTime + ", orderNo="+ orderNo + "]";
    }
}