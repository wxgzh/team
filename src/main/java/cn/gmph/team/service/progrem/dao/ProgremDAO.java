package cn.gmph.team.service.progrem.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import cn.gmph.team.service.progrem.entity.ProgremDO;
import cn.gmph.team.service.progrem.dao.helper.ProgremProvider;

import java.util.List;;

public interface ProgremDAO {

    @Select("SELECT * FROM team_progrem WHERE id = #{id}")
    public ProgremDO getById(@Param("id") int id);

    @Insert("INSERT into team_progrem(id,name,imagePath,descript,voteDesc,isSignIn,lastUpdateUserId,updateTime,orderNo) VALUES (#{id},#{name},#{imagePath},#{descript},#{voteDesc},#{isSignIn},#{lastUpdateUserId},#{updateTime},#{orderNo})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    public void insert(ProgremDO progrem);

    @Delete("DELETE FROM team_progrem WHERE id = #{id}")
    public int deleteById(@Param("id") int id);

    @UpdateProvider(type = ProgremProvider.class, method = "update")
    public int update(@Param("progrem") ProgremDO  progrem);

    @SelectProvider(type = ProgremProvider.class, method = "pageList")
    public List<ProgremDO> pageList(@Param("progrem") ProgremDO progrem, @Param("pageNum") Integer pageNum, @Param("pageSize") Integer pageSize);

    @SelectProvider(type = ProgremProvider.class, method = "pageListCount")
    public Integer pageListCount(@Param("progrem") ProgremDO progrem);

}