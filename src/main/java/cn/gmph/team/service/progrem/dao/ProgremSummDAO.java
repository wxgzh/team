package cn.gmph.team.service.progrem.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import cn.gmph.team.service.progrem.entity.ProgremSummDO;
import cn.gmph.team.service.progrem.dao.helper.ProgremSummProvider;

import java.util.List;;

public interface ProgremSummDAO {

    @Select("SELECT * FROM team_progrem_summ WHERE id = #{id}")
    public ProgremSummDO getById(@Param("id") int id);

    @Insert("INSERT into team_progrem_summ(id,descript,isShow,createId,lastUpdateTime) VALUES (#{id},#{descript},#{isShow},#{createId},#{lastUpdateTime})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    public void insert(ProgremSummDO progremSumm);

    @Delete("DELETE FROM team_progrem_summ WHERE id = #{id}")
    public int deleteById(@Param("id") int id);

    @UpdateProvider(type = ProgremSummProvider.class, method = "update")
    public int update(@Param("progremSumm") ProgremSummDO  progremSumm);

    @SelectProvider(type = ProgremSummProvider.class, method = "pageList")
    public List<ProgremSummDO> pageList(@Param("progremSumm") ProgremSummDO progremSumm, @Param("pageNum") Integer pageNum, @Param("pageSize") Integer pageSize);

    @SelectProvider(type = ProgremSummProvider.class, method = "pageListCount")
    public Integer pageListCount(@Param("progremSumm") ProgremSummDO progremSumm);

}