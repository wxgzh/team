package cn.gmph.team.service.progrem;

import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.gmph.team.service.progrem.dao.ProgremDAO;
import cn.gmph.team.service.progrem.entity.ProgremDO;
import cn.gmph.common.base.BaseService;
import cn.gmph.common.base.ResultDTO;
import cn.gmph.common.base.ResultPageDTO;

@Service
public class ProgremService extends BaseService {

 private Logger logger = LoggerFactory.getLogger(this.getClass());
 @Autowired
 private ProgremDAO progremDAO;

 // 分页
 public ResultPageDTO<ProgremDO> page(ProgremDO progrem, Integer pageNum, Integer pageSize) {
     logger.info("开始分页查询ProgremService.page, progrem=" + progrem.toString());
     List<ProgremDO> pageList = this.progremDAO.pageList(progrem, pageNum, pageSize);
     Integer count = this.progremDAO.pageListCount(progrem);
   ResultPageDTO<ProgremDO> pager =  new ResultPageDTO<ProgremDO>(count,pageList);
     return pager;
 }

 // 添加
 public ProgremDO doAdd (ProgremDO progrem,int loginUserId) {
     logger.info("开始添加ProgremService.add,progrem=" + progrem.toString());
     this.progremDAO.insert(progrem);
     return progrem;
 }

 // 修改
 public Integer doUpdate (ProgremDO progrem,Integer loginUserId) {
     logger.info("开始修改ProgremService.update,progrem=" + progrem.toString());
     int rows=this.progremDAO.update(progrem);
     return rows;
 }

 // 删除
 public Integer doDelete (ProgremDO progrem,Integer loginUserId) {
     logger.info("开始删除ProgremService.delete,progrem=" + progrem.toString());
     int rows=this.progremDAO.deleteById(progrem.getId());
     return rows;
 }

 // 查询
 public ProgremDO doQueryById (Integer id) {
     ProgremDO obj = this.progremDAO.getById(id);
     return obj;
 }
}