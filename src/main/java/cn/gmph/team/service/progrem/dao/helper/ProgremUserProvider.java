package cn.gmph.team.service.progrem.dao.helper;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang3.StringUtils;

import cn.gmph.team.service.progrem.entity.ProgremUserDO;
public class ProgremUserProvider {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String TABLE_NAME = "team_progrem_user";

    public String update(Map<String, Object> params) {
        ProgremUserDO progremUser = (ProgremUserDO) params.get("progremUser");
        BEGIN();
        UPDATE(TABLE_NAME);
        if (progremUser.getProgremId() != null) {
            SET("progremId=#{progremUser.progremId}");
        }
        if (progremUser.getUserId() != null) {
            SET("userId=#{progremUser.userId}");
        }
        if (progremUser.getJoinTime() != null) {
            SET("joinTime=#{progremUser.joinTime}");
        }
        if (progremUser.getStatus() != null) {
            SET("status=#{progremUser.status}");
        }
        if (progremUser.getCreateTime() != null) {
            SET("createTime=#{progremUser.createTime}");
        }
        WHERE("id = #{progremUser.id}");
        String sql = SQL();
        return sql;
    }

    public String pageList(Map<String, Object> params) {
        ProgremUserDO progremUser = (ProgremUserDO) params.get("progremUser");
        Integer pageNum = (Integer) params.get("pageNum");
        Integer pageSize = (Integer) params.get("pageSize");
        BEGIN();
        SELECT("*");
        FROM(TABLE_NAME);
        if (progremUser.getId() != null) {
            WHERE("id=#{progremUser.id}");
        }
        if (progremUser.getProgremId() != null) {
            WHERE("progremId=#{progremUser.progremId}");
        }
        if (progremUser.getUserId() != null) {
            WHERE("userId=#{progremUser.userId}");
        }
        if (progremUser.getJoinTime() != null) {
            WHERE("joinTime=#{progremUser.joinTime}");
        }
        if (progremUser.getStatus() != null) {
            WHERE("status=#{progremUser.status}");
        }
        if (progremUser.getCreateTime() != null) {
            WHERE("createTime=#{progremUser.createTime}");
        }
        ORDER_BY("id desc");
        String sql = SQL();
        int start = 0;
        int limit = 0;
        if (pageNum == null || pageNum == 0) {
            pageNum = 1;
        }
        if (pageSize == null || pageSize == 0) {
            pageSize = 20;
        }
        start = (pageNum - 1) * pageSize;
        limit = pageSize;
        sql += " limit " + start + ", " + limit;
        return sql;
    }

    public String pageListCount(Map<String, Object> params) {
        ProgremUserDO progremUser = (ProgremUserDO) params.get("progremUser");
        BEGIN();
        SELECT("count(1)");
        FROM(TABLE_NAME);
        if (progremUser.getId() != null) {
            WHERE("id=#{progremUser.id}");
        }
        if (progremUser.getProgremId() != null) {
            WHERE("progremId=#{progremUser.progremId}");
        }
        if (progremUser.getUserId() != null) {
            WHERE("userId=#{progremUser.userId}");
        }
        if (progremUser.getJoinTime() != null) {
            WHERE("joinTime=#{progremUser.joinTime}");
        }
        if (progremUser.getStatus() != null) {
            WHERE("status=#{progremUser.status}");
        }
        if (progremUser.getCreateTime() != null) {
            WHERE("createTime=#{progremUser.createTime}");
        }
        String sql = SQL();
        return sql;
    }
}

