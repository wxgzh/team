package cn.gmph.team.service.progrem;

import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.gmph.team.service.progrem.dao.ProgremPenaltyDAO;
import cn.gmph.team.service.progrem.entity.ProgremPenaltyDO;
import cn.gmph.common.base.BaseService;
import cn.gmph.common.base.ResultDTO;
import cn.gmph.common.base.ResultPageDTO;

@Service
public class ProgremPenaltyService extends BaseService {

    private Logger            logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private ProgremPenaltyDAO progremPenaltyDAO;

    // 分页
    public ResultPageDTO<ProgremPenaltyDO> page(ProgremPenaltyDO progremPenalty, Integer pageNum, Integer pageSize) {
        logger.info("开始分页查询ProgremPenaltyService.page, progremPenalty=" + progremPenalty.toString());
        List<ProgremPenaltyDO> pageList = this.progremPenaltyDAO.pageList(progremPenalty, pageNum, pageSize);
        Integer count = this.progremPenaltyDAO.pageListCount(progremPenalty);
        ResultPageDTO<ProgremPenaltyDO> pager = new ResultPageDTO<ProgremPenaltyDO>(count, pageList);
        return pager;
    }

    // 添加
    public ProgremPenaltyDO doAdd(ProgremPenaltyDO progremPenalty, int loginUserId) {
        logger.info("开始添加ProgremPenaltyService.add,progremPenalty=" + progremPenalty.toString());
        this.progremPenaltyDAO.insert(progremPenalty);
        return progremPenalty;
    }

    // 修改
    public Integer doUpdate(ProgremPenaltyDO progremPenalty, Integer loginUserId) {
        logger.info("开始修改ProgremPenaltyService.update,progremPenalty=" + progremPenalty.toString());
        int rows = this.progremPenaltyDAO.update(progremPenalty);
        return rows;
    }

    // 删除
    public Integer doDelete(ProgremPenaltyDO progremPenalty, Integer loginUserId) {
        logger.info("开始删除ProgremPenaltyService.delete,progremPenalty=" + progremPenalty.toString());
        int rows = this.progremPenaltyDAO.deleteById(progremPenalty.getId());
        return rows;
    }

    // 查询
    public ProgremPenaltyDO doQueryById(Integer id) {
        ProgremPenaltyDO obj = this.progremPenaltyDAO.getById(id);
        return obj;
    }

    // 分页
    public ResultPageDTO<ProgremPenaltyDO> punishList(ProgremPenaltyDO progremPenalty, Integer pageNum, Integer pageSize) {
        logger.info("开始分页查询ProgremPenaltyService.page, progremPenalty=" + progremPenalty.toString());
        progremPenalty.setStatus(0);
        List<ProgremPenaltyDO> pageList = this.progremPenaltyDAO.pageList(progremPenalty, pageNum, pageSize);
        Integer count = this.progremPenaltyDAO.pageListCount(progremPenalty);
        ResultPageDTO<ProgremPenaltyDO> pager = new ResultPageDTO<ProgremPenaltyDO>(count, pageList);
        return pager;
    }
}