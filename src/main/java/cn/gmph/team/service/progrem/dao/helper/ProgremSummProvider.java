package cn.gmph.team.service.progrem.dao.helper;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang3.StringUtils;

import cn.gmph.team.service.progrem.entity.ProgremSummDO;
public class ProgremSummProvider {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String TABLE_NAME = "team_progrem_summ";

    public String update(Map<String, Object> params) {
        ProgremSummDO progremSumm = (ProgremSummDO) params.get("progremSumm");
        BEGIN();
        UPDATE(TABLE_NAME);
        if (StringUtils.isNotBlank(progremSumm.getDescript())){
            SET("descript=#{progremSumm.descript}");
        }
        if (progremSumm.getIsShow() != null) {
            SET("isShow=#{progremSumm.isShow}");
        }
        if (progremSumm.getCreateId() != null) {
            SET("createId=#{progremSumm.createId}");
        }
        if (progremSumm.getLastUpdateTime() != null) {
            SET("lastUpdateTime=#{progremSumm.lastUpdateTime}");
        }
        WHERE("id = #{progremSumm.id}");
        String sql = SQL();
        return sql;
    }

    public String pageList(Map<String, Object> params) {
        ProgremSummDO progremSumm = (ProgremSummDO) params.get("progremSumm");
        Integer pageNum = (Integer) params.get("pageNum");
        Integer pageSize = (Integer) params.get("pageSize");
        BEGIN();
        SELECT("*");
        FROM(TABLE_NAME);
        if (progremSumm.getId() != null) {
            WHERE("id=#{progremSumm.id}");
        }
        if (StringUtils.isNotBlank(progremSumm.getDescript())){
            WHERE("descript=#{progremSumm.descript}");
        }
        if (progremSumm.getIsShow() != null) {
            WHERE("isShow=#{progremSumm.isShow}");
        }
        if (progremSumm.getCreateId() != null) {
            WHERE("createId=#{progremSumm.createId}");
        }
        if (progremSumm.getLastUpdateTime() != null) {
            WHERE("lastUpdateTime=#{progremSumm.lastUpdateTime}");
        }
        ORDER_BY("id desc");
        String sql = SQL();
        int start = 0;
        int limit = 0;
        if (pageNum == null || pageNum == 0) {
            pageNum = 1;
        }
        if (pageSize == null || pageSize == 0) {
            pageSize = 20;
        }
        start = (pageNum - 1) * pageSize;
        limit = pageSize;
        sql += " limit " + start + ", " + limit;
        return sql;
    }

    public String pageListCount(Map<String, Object> params) {
        ProgremSummDO progremSumm = (ProgremSummDO) params.get("progremSumm");
        BEGIN();
        SELECT("count(1)");
        FROM(TABLE_NAME);
        if (progremSumm.getId() != null) {
            WHERE("id=#{progremSumm.id}");
        }
        if (StringUtils.isNotBlank(progremSumm.getDescript())){
            WHERE("descript=#{progremSumm.descript}");
        }
        if (progremSumm.getIsShow() != null) {
            WHERE("isShow=#{progremSumm.isShow}");
        }
        if (progremSumm.getCreateId() != null) {
            WHERE("createId=#{progremSumm.createId}");
        }
        if (progremSumm.getLastUpdateTime() != null) {
            WHERE("lastUpdateTime=#{progremSumm.lastUpdateTime}");
        }
        String sql = SQL();
        return sql;
    }
}

