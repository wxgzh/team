package cn.gmph.team.service.progrem;

import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.gmph.team.service.progrem.dao.ProgremSignDAO;
import cn.gmph.team.service.progrem.entity.ProgremSignDO;
import cn.gmph.common.base.BaseService;
import cn.gmph.common.base.ResultDTO;
import cn.gmph.common.base.ResultPageDTO;

@Service
public class ProgremSignService extends BaseService {

    private Logger         logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private ProgremSignDAO progremSignDAO;

    // 分页
    public ResultPageDTO<ProgremSignDO> page(ProgremSignDO progremSign, Integer pageNum, Integer pageSize) {
        logger.info("开始分页查询ProgremSignService.page, progremSign=" + progremSign.toString());
        List<ProgremSignDO> pageList = this.progremSignDAO.pageList(progremSign, pageNum, pageSize);
        Integer count = this.progremSignDAO.pageListCount(progremSign);
        ResultPageDTO<ProgremSignDO> pager = new ResultPageDTO<ProgremSignDO>(count, pageList);
        return pager;
    }

    // 添加
    public ProgremSignDO doAdd(ProgremSignDO progremSign, int loginUserId) {
        logger.info("开始添加ProgremSignService.add,progremSign=" + progremSign.toString());
        this.progremSignDAO.insert(progremSign);
        return progremSign;
    }

    // 修改
    public Integer doUpdate(ProgremSignDO progremSign, Integer loginUserId) {
        logger.info("开始修改ProgremSignService.update,progremSign=" + progremSign.toString());
        int rows = this.progremSignDAO.update(progremSign);
        return rows;
    }

    // 删除
    public Integer doDelete(ProgremSignDO progremSign, Integer loginUserId) {
        logger.info("开始删除ProgremSignService.delete,progremSign=" + progremSign.toString());
        int rows = this.progremSignDAO.deleteById(progremSign.getId());
        return rows;
    }

    // 查询
    public ProgremSignDO doQueryById(Integer id) {
        ProgremSignDO obj = this.progremSignDAO.getById(id);
        return obj;
    }

    public List<ProgremSignDO> doQueryByUserId(Integer userId, Integer progremId) {
        List<ProgremSignDO> obj = this.progremSignDAO.getByUserId(userId, progremId);
        return obj;
    }
}