package cn.gmph.team.service.progrem.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import cn.gmph.team.service.progrem.entity.ProgremPenaltyDO;
import cn.gmph.team.service.progrem.dao.helper.ProgremPenaltyProvider;

import java.util.List;;

public interface ProgremPenaltyDAO {

    @Select("SELECT * FROM team_progrem_penalty WHERE id = #{id}")
    public ProgremPenaltyDO getById(@Param("id") int id);

    @Insert("INSERT into team_progrem_penalty(id,userId,describ,status) VALUES (#{id},#{userId},#{describ},#{status})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    public void insert(ProgremPenaltyDO progremPenalty);

    @Delete("DELETE FROM team_progrem_penalty WHERE id = #{id}")
    public int deleteById(@Param("id") int id);

    @UpdateProvider(type = ProgremPenaltyProvider.class, method = "update")
    public int update(@Param("progremPenalty") ProgremPenaltyDO  progremPenalty);

    @SelectProvider(type = ProgremPenaltyProvider.class, method = "pageList")
    public List<ProgremPenaltyDO> pageList(@Param("progremPenalty") ProgremPenaltyDO progremPenalty, @Param("pageNum") Integer pageNum, @Param("pageSize") Integer pageSize);

    @SelectProvider(type = ProgremPenaltyProvider.class, method = "pageListCount")
    public Integer pageListCount(@Param("progremPenalty") ProgremPenaltyDO progremPenalty);

}