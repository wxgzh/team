package cn.gmph.team.service.progrem;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gmph.common.base.BaseService;
import cn.gmph.common.base.ResultPageDTO;
import cn.gmph.team.service.progrem.dao.ProgremUserDAO;
import cn.gmph.team.service.progrem.entity.ProgremDO;
import cn.gmph.team.service.progrem.entity.ProgremUserDO;
import cn.gmph.team.service.sys.VoteExtService;
import cn.gmph.team.service.sys.VoteService;
import cn.gmph.team.service.sys.entity.VoteDO;
import cn.gmph.team.service.sys.entity.VoteExtDO;

@Service
public class ProgremUserService extends BaseService {

    private Logger         logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private ProgremUserDAO progremUserDAO;
    @Autowired
    private VoteService    voteService;
    @Autowired
    private ProgremService progremService;
    @Autowired
    private VoteExtService voteExtService;

    // 分页
    public ResultPageDTO<ProgremUserDO> page(ProgremUserDO progremUser, Integer pageNum, Integer pageSize) {
        logger.info("开始分页查询ProgremUserService.page, progremUser=" + progremUser.toString());
        List<ProgremUserDO> pageList = this.progremUserDAO.pageList(progremUser, pageNum, pageSize);
        Integer count = this.progremUserDAO.pageListCount(progremUser);
        ResultPageDTO<ProgremUserDO> pager = new ResultPageDTO<ProgremUserDO>(count, pageList);
        return pager;
    }

    // 添加
    @Transactional
    public ProgremUserDO doAdd(ProgremUserDO progremUser, int loginUserId) {
        logger.info("开始添加ProgremUserService.add,progremUser=" + progremUser.toString());
        this.progremUserDAO.insert(progremUser);
        ProgremDO progremDO = progremService.doQueryById(progremUser.getProgremId());
        VoteDO vote = new VoteDO();
        vote.setCreateUserId(progremUser.getId());
        vote.setResult(0);
        vote.setCreateTime(new Date());
        vote.setType(1);
        vote.setTypeId(progremUser.getId());
        vote.setVoteDesc(progremUser.getUserName() + "申请加入《" + progremDO.getName() + "》项目");
        voteService.doAdd(vote, progremUser.getUserId());
        VoteExtDO voteExt = new VoteExtDO();
        voteExt.setName("同意");
        voteExt.setValue("1");
        voteExt.setVoteId(vote.getId());
        voteExtService.doAdd(voteExt, progremUser.getUserId());
        voteExt = new VoteExtDO();
        voteExt.setName("不同意");
        voteExt.setValue("2");
        voteExt.setVoteId(vote.getId());
        voteExtService.doAdd(voteExt, progremUser.getUserId());
        return progremUser;
    }

    // 修改
    public Integer doUpdate(ProgremUserDO progremUser, Integer loginUserId) {
        logger.info("开始修改ProgremUserService.update,progremUser=" + progremUser.toString());
        int rows = this.progremUserDAO.update(progremUser);
        return rows;
    }

    // 删除
    public Integer doDelete(ProgremUserDO progremUser, Integer loginUserId) {
        logger.info("开始删除ProgremUserService.delete,progremUser=" + progremUser.toString());
        int rows = this.progremUserDAO.deleteById(progremUser.getId());
        return rows;
    }

    // 查询
    public ProgremUserDO doQueryById(Integer id) {
        ProgremUserDO obj = this.progremUserDAO.getById(id);
        return obj;
    }

    public ProgremUserDO doQueryByUserId(Integer userId, Integer progremId) {
        ProgremUserDO obj = this.progremUserDAO.getByUserId(userId, progremId);
        return obj;
    }

    // 分页
    public ResultPageDTO<ProgremUserDO> joinUserList(ProgremUserDO progremUser, Integer pageNum, Integer pageSize) {
        logger.info("开始分页查询ProgremUserService.page, progremUser=" + progremUser.toString());
        progremUser.setStatus(1);//申请未投票的记录
        //状态1申请加入2申请通过3申请未通过0退出
        List<ProgremUserDO> pageList = this.progremUserDAO.pageList(progremUser, pageNum, pageSize);
        Integer count = this.progremUserDAO.pageListCount(progremUser);
        ResultPageDTO<ProgremUserDO> pager = new ResultPageDTO<ProgremUserDO>(count, pageList);
        return pager;
    }
}