package cn.gmph.team.service.progrem.dao.helper;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang3.StringUtils;

import cn.gmph.team.service.progrem.entity.ProgremPenaltyDO;
public class ProgremPenaltyProvider {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String TABLE_NAME = "team_progrem_penalty";

    public String update(Map<String, Object> params) {
        ProgremPenaltyDO progremPenalty = (ProgremPenaltyDO) params.get("progremPenalty");
        BEGIN();
        UPDATE(TABLE_NAME);
        if (progremPenalty.getUserId() != null) {
            SET("userId=#{progremPenalty.userId}");
        }
        if (StringUtils.isNotBlank(progremPenalty.getDescrib())){
            SET("describ=#{progremPenalty.describ}");
        }
        if (progremPenalty.getStatus() != null) {
            SET("status=#{progremPenalty.status}");
        }
        WHERE("id = #{progremPenalty.id}");
        String sql = SQL();
        return sql;
    }

    public String pageList(Map<String, Object> params) {
        ProgremPenaltyDO progremPenalty = (ProgremPenaltyDO) params.get("progremPenalty");
        Integer pageNum = (Integer) params.get("pageNum");
        Integer pageSize = (Integer) params.get("pageSize");
        BEGIN();
        SELECT("*");
        FROM(TABLE_NAME);
        if (progremPenalty.getId() != null) {
            WHERE("id=#{progremPenalty.id}");
        }
        if (progremPenalty.getUserId() != null) {
            WHERE("userId=#{progremPenalty.userId}");
        }
        if (StringUtils.isNotBlank(progremPenalty.getDescrib())){
            WHERE("describ=#{progremPenalty.describ}");
        }
        if (progremPenalty.getStatus() != null) {
            WHERE("status=#{progremPenalty.status}");
        }
        ORDER_BY("id desc");
        String sql = SQL();
        int start = 0;
        int limit = 0;
        if (pageNum == null || pageNum == 0) {
            pageNum = 1;
        }
        if (pageSize == null || pageSize == 0) {
            pageSize = 20;
        }
        start = (pageNum - 1) * pageSize;
        limit = pageSize;
        sql += " limit " + start + ", " + limit;
        return sql;
    }

    public String pageListCount(Map<String, Object> params) {
        ProgremPenaltyDO progremPenalty = (ProgremPenaltyDO) params.get("progremPenalty");
        BEGIN();
        SELECT("count(1)");
        FROM(TABLE_NAME);
        if (progremPenalty.getId() != null) {
            WHERE("id=#{progremPenalty.id}");
        }
        if (progremPenalty.getUserId() != null) {
            WHERE("userId=#{progremPenalty.userId}");
        }
        if (StringUtils.isNotBlank(progremPenalty.getDescrib())){
            WHERE("describ=#{progremPenalty.describ}");
        }
        if (progremPenalty.getStatus() != null) {
            WHERE("status=#{progremPenalty.status}");
        }
        String sql = SQL();
        return sql;
    }
}

