package cn.gmph.team.service.progrem;

import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.gmph.team.service.progrem.dao.ProgremSummDAO;
import cn.gmph.team.service.progrem.entity.ProgremSummDO;
import cn.gmph.common.base.BaseService;
import cn.gmph.common.base.ResultDTO;
import cn.gmph.common.base.ResultPageDTO;

@Service
public class ProgremSummService extends BaseService {

 private Logger logger = LoggerFactory.getLogger(this.getClass());
 @Autowired
 private ProgremSummDAO progremSummDAO;

 // 分页
 public ResultPageDTO<ProgremSummDO> page(ProgremSummDO progremSumm, Integer pageNum, Integer pageSize) {
     logger.info("开始分页查询ProgremSummService.page, progremSumm=" + progremSumm.toString());
     List<ProgremSummDO> pageList = this.progremSummDAO.pageList(progremSumm, pageNum, pageSize);
     Integer count = this.progremSummDAO.pageListCount(progremSumm);
   ResultPageDTO<ProgremSummDO> pager =  new ResultPageDTO<ProgremSummDO>(count,pageList);
     return pager;
 }

 // 添加
 public ProgremSummDO doAdd (ProgremSummDO progremSumm,int loginUserId) {
     logger.info("开始添加ProgremSummService.add,progremSumm=" + progremSumm.toString());
     this.progremSummDAO.insert(progremSumm);
     return progremSumm;
 }

 // 修改
 public Integer doUpdate (ProgremSummDO progremSumm,Integer loginUserId) {
     logger.info("开始修改ProgremSummService.update,progremSumm=" + progremSumm.toString());
     int rows=this.progremSummDAO.update(progremSumm);
     return rows;
 }

 // 删除
 public Integer doDelete (ProgremSummDO progremSumm,Integer loginUserId) {
     logger.info("开始删除ProgremSummService.delete,progremSumm=" + progremSumm.toString());
     int rows=this.progremSummDAO.deleteById(progremSumm.getId());
     return rows;
 }

 // 查询
 public ProgremSummDO doQueryById (Integer id) {
     ProgremSummDO obj = this.progremSummDAO.getById(id);
     return obj;
 }
}