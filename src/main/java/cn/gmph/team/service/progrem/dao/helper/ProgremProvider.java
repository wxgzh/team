package cn.gmph.team.service.progrem.dao.helper;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang3.StringUtils;

import cn.gmph.team.service.progrem.entity.ProgremDO;
public class ProgremProvider {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String TABLE_NAME = "team_progrem";

    public String update(Map<String, Object> params) {
        ProgremDO progrem = (ProgremDO) params.get("progrem");
        BEGIN();
        UPDATE(TABLE_NAME);
        if (StringUtils.isNotBlank(progrem.getName())){
            SET("name=#{progrem.name}");
        }
        if (StringUtils.isNotBlank(progrem.getImagePath())){
            SET("imagePath=#{progrem.imagePath}");
        }
        if (StringUtils.isNotBlank(progrem.getDescript())){
            SET("descript=#{progrem.descript}");
        }
        if (StringUtils.isNotBlank(progrem.getVoteDesc())){
            SET("voteDesc=#{progrem.voteDesc}");
        }
        if (progrem.getIsSignIn() != null) {
            SET("isSignIn=#{progrem.isSignIn}");
        }
        if (progrem.getLastUpdateUserId() != null) {
            SET("lastUpdateUserId=#{progrem.lastUpdateUserId}");
        }
        if (progrem.getUpdateTime() != null) {
            SET("updateTime=#{progrem.updateTime}");
        }
        if (progrem.getOrderNo() != null) {
            SET("orderNo=#{progrem.orderNo}");
        }
        WHERE("id = #{progrem.id}");
        String sql = SQL();
        return sql;
    }

    public String pageList(Map<String, Object> params) {
        ProgremDO progrem = (ProgremDO) params.get("progrem");
        Integer pageNum = (Integer) params.get("pageNum");
        Integer pageSize = (Integer) params.get("pageSize");
        BEGIN();
        SELECT("*");
        FROM(TABLE_NAME);
        if (progrem.getId() != null) {
            WHERE("id=#{progrem.id}");
        }
        if (StringUtils.isNotBlank(progrem.getName())){
            WHERE("name=#{progrem.name}");
        }
        if (StringUtils.isNotBlank(progrem.getImagePath())){
            WHERE("imagePath=#{progrem.imagePath}");
        }
        if (StringUtils.isNotBlank(progrem.getDescript())){
            WHERE("descript=#{progrem.descript}");
        }
        if (StringUtils.isNotBlank(progrem.getVoteDesc())){
            WHERE("voteDesc=#{progrem.voteDesc}");
        }
        if (progrem.getIsSignIn() != null) {
            WHERE("isSignIn=#{progrem.isSignIn}");
        }
        if (progrem.getLastUpdateUserId() != null) {
            WHERE("lastUpdateUserId=#{progrem.lastUpdateUserId}");
        }
        if (progrem.getUpdateTime() != null) {
            WHERE("updateTime=#{progrem.updateTime}");
        }
        if (progrem.getOrderNo() != null) {
            WHERE("orderNo=#{progrem.orderNo}");
        }
        ORDER_BY("id desc");
        String sql = SQL();
        int start = 0;
        int limit = 0;
        if (pageNum == null || pageNum == 0) {
            pageNum = 1;
        }
        if (pageSize == null || pageSize == 0) {
            pageSize = 20;
        }
        start = (pageNum - 1) * pageSize;
        limit = pageSize;
        sql += " limit " + start + ", " + limit;
        return sql;
    }

    public String pageListCount(Map<String, Object> params) {
        ProgremDO progrem = (ProgremDO) params.get("progrem");
        BEGIN();
        SELECT("count(1)");
        FROM(TABLE_NAME);
        if (progrem.getId() != null) {
            WHERE("id=#{progrem.id}");
        }
        if (StringUtils.isNotBlank(progrem.getName())){
            WHERE("name=#{progrem.name}");
        }
        if (StringUtils.isNotBlank(progrem.getImagePath())){
            WHERE("imagePath=#{progrem.imagePath}");
        }
        if (StringUtils.isNotBlank(progrem.getDescript())){
            WHERE("descript=#{progrem.descript}");
        }
        if (StringUtils.isNotBlank(progrem.getVoteDesc())){
            WHERE("voteDesc=#{progrem.voteDesc}");
        }
        if (progrem.getIsSignIn() != null) {
            WHERE("isSignIn=#{progrem.isSignIn}");
        }
        if (progrem.getLastUpdateUserId() != null) {
            WHERE("lastUpdateUserId=#{progrem.lastUpdateUserId}");
        }
        if (progrem.getUpdateTime() != null) {
            WHERE("updateTime=#{progrem.updateTime}");
        }
        if (progrem.getOrderNo() != null) {
            WHERE("orderNo=#{progrem.orderNo}");
        }
        String sql = SQL();
        return sql;
    }
}

