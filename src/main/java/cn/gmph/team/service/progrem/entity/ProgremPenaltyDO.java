package cn.gmph.team.service.progrem.entity;

public class ProgremPenaltyDO {

    /**
     * 惩罚ID
     */
    private Integer id;

    /**
     * 惩罚人员ID
     */
    private Integer userId;

    /**
     * 惩罚说明
     */
    private String  describ;

    /**
     * 惩罚状态
     */
    private Integer status;

    //惩罚人员名称
    private String  userName;

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getDescrib() {
        return describ;
    }

    public void setDescrib(String describ) {
        this.describ = describ;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "[id=" + id + ", userId=" + userId + ", describ=" + describ + ", status=" + status + "]";
    }
}