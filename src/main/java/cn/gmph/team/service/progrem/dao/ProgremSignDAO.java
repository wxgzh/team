package cn.gmph.team.service.progrem.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import cn.gmph.team.service.progrem.entity.ProgremSignDO;
import cn.gmph.team.service.progrem.dao.helper.ProgremSignProvider;

import java.util.List;;

public interface ProgremSignDAO {

    @Select("SELECT * FROM team_progrem_sign WHERE id = #{id}")
    public ProgremSignDO getById(@Param("id") int id);
    @Select("SELECT * FROM team_progrem_sign WHERE userId = #{userId} and progremId= #{progremId} and to_days(signTime) = to_days(now()) ")
    public List<ProgremSignDO> getByUserId(@Param("userId") Integer userId, @Param("progremId") Integer progremId);
    
    @Insert("INSERT into team_progrem_sign(id,userId,progremId,signTime) VALUES (#{id},#{userId},#{progremId},#{signTime})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    public void insert(ProgremSignDO progremSign);

    @Delete("DELETE FROM team_progrem_sign WHERE id = #{id}")
    public int deleteById(@Param("id") int id);

    @UpdateProvider(type = ProgremSignProvider.class, method = "update")
    public int update(@Param("progremSign") ProgremSignDO  progremSign);

    @SelectProvider(type = ProgremSignProvider.class, method = "pageList")
    public List<ProgremSignDO> pageList(@Param("progremSign") ProgremSignDO progremSign, @Param("pageNum") Integer pageNum, @Param("pageSize") Integer pageSize);

    @SelectProvider(type = ProgremSignProvider.class, method = "pageListCount")
    public Integer pageListCount(@Param("progremSign") ProgremSignDO progremSign);

}