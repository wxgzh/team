package cn.gmph.team.service.progrem.entity;

import java.util.Date;

public class ProgremSignDO {

    /**
     * 签到ID
     */
    private Integer id;

    /**
     * 签到用户ID
     */
    private Integer userId;

    /**
     * 签到项目
     */
    private Integer progremId;

    /**
     * 签到时间
     */
    private Date signTime;

    private Date startTime;
    private Date endTime;


    /**
     * @return the startTime
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the endTime
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getProgremId() {
        return progremId;
    }

    public void setProgremId(Integer progremId) {
        this.progremId = progremId;
    }

    public Date getSignTime() {
        return signTime;
    }

    public void setSignTime(Date signTime) {
        this.signTime = signTime;
    }



    @Override
    public String toString() {
        return "[id="+ id + ", userId="+ userId + ", progremId="+ progremId + ", signTime="+ signTime + "]";
    }
}