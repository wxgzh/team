package cn.gmph.team.service.progrem.entity;

import java.util.Date;

public class ProgremSummDO {

    /**
     * 分类ID
     */
    private Integer id;

    /**
     * 分类描述
     */
    private String  descript;

    /**
     * 是否可见
     */
    private Integer isShow;

    /**
     * 总结人
     */
    private Integer createId;

    /**
     * 最后修改时间
     */
    private Date    lastUpdateTime;
    //用户名
    private String  name;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

    public Integer getCreateId() {
        return createId;
    }

    public void setCreateId(Integer createId) {
        this.createId = createId;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    @Override
    public String toString() {
        return "[id=" + id + ", descript=" + descript + ", isShow=" + isShow + ", createId=" + createId + ", lastUpdateTime=" + lastUpdateTime + "]";
    }
}