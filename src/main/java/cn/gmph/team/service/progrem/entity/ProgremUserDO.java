package cn.gmph.team.service.progrem.entity;

import java.util.Date;

public class ProgremUserDO {

    /**
     * 项目人员ID
     */
    private Integer id;

    /**
     * 项目ID
     */
    private Integer progremId;

    /**
     * 人员ID
     */
    private Integer userId;

    /**
     * 加入时间
     */
    private Date    joinTime;

    /**
     * 状态1加入2待投票0退出
     */
    private Integer status;

    /**
     * 申请时间
     */
    private Date    createTime;

    //申请项目名称
    private String  progremName;
    //申请人名称
    private String  userName;
    //投票描述
    private String  descript;

    /**
     * @return the progremName
     */
    public String getProgremName() {
        return progremName;
    }

    /**
     * @param progremName the progremName to set
     */
    public void setProgremName(String progremName) {
        this.progremName = progremName;
    }

    /**
     * @return the descript
     */
    public String getDescript() {
        return descript;
    }

    /**
     * @param descript the descript to set
     */
    public void setDescript(String descript) {
        this.descript = descript;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProgremId() {
        return progremId;
    }

    public void setProgremId(Integer progremId) {
        this.progremId = progremId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(Date joinTime) {
        this.joinTime = joinTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "[id=" + id + ", progremId=" + progremId + ", userId=" + userId + ", joinTime=" + joinTime + ", status=" + status + ", createTime=" + createTime + "]";
    }
}