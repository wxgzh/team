package cn.gmph.team.web.progrem;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.gmph.common.base.BaseController;
import cn.gmph.common.base.ResultDTO;
import cn.gmph.common.base.ResultPageDTO;
import cn.gmph.team.service.progrem.ProgremService;
import cn.gmph.team.service.progrem.ProgremUserService;
import cn.gmph.team.service.progrem.entity.ProgremDO;
import cn.gmph.team.service.progrem.entity.ProgremUserDO;
import cn.gmph.team.service.sys.UserBiz;
import cn.gmph.team.service.sys.VoteExtService;
import cn.gmph.team.service.sys.VoteService;
import cn.gmph.team.service.sys.entity.VoteDO;
import cn.gmph.team.service.sys.entity.VoteExtDO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping(value = "/progremUser", produces = "text/html;charset=utf-8", method = RequestMethod.POST)
@Api(value = "/progremUser", tags = { "项目_人员" })
public class ProgremUserController extends BaseController {

    @Autowired
    private ProgremUserService progremUserService;
    

    // 列表页
    @RequestMapping("/list.htm")
    public String list() {
        return "";
    }

    // 分页
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @ResponseBody
    @RequestMapping(value = "query.htm", method = RequestMethod.GET)
    public ResultDTO page(@RequestBody ProgremUserDO progremUser) {
        logger.info("开始分页查询ProgremUserController.page, progremUser=" + progremUser.toString());
        Map<String, Integer> params = super.copyParamsToInteger(new String[] { "page", "rows" });
        Integer page = params.get("page");
        Integer rows = params.get("rows");
        ResultPageDTO<ProgremUserDO> pager = this.progremUserService.page(progremUser, page, rows);
        return success(pager);
    }

    // 添加
    @ApiOperation(value = "新增保存", notes = "新增保存")
    @ResponseBody
    @RequestMapping(value = "doAdd.htm")
    public ResultDTO doAdd(@RequestBody ProgremUserDO progremUser) {
        ProgremUserDO resultDO = this.progremUserService.doAdd(progremUser, super.getUserId());
        return success(resultDO);
    }

    // 修改
    @ApiOperation(value = "修改保存", notes = "修改保存")
    @ResponseBody
    @RequestMapping(value = "doUpdate.htm")
    public ResultDTO doUpdate(@RequestBody ProgremUserDO progremUser) {
        Integer result = this.progremUserService.doUpdate(progremUser, super.getUserId());
        return success(result);
    }

    // 删除
    @ApiOperation(value = "删除", notes = "删除")
    @ResponseBody
    @RequestMapping(value = "doDelete.htm")
    public ResultDTO doDelete(@RequestBody ProgremUserDO progremUser) {
        Integer result = this.progremUserService.doDelete(progremUser, super.getUserId());
        return success(result);
    }

    // 详情
    @ApiOperation(value = "查询详情", notes = "查询详情")
    @ResponseBody
    @RequestMapping(value = "detail.htm")
    public ResultDTO detail(@RequestParam String id) {
        ProgremUserDO result = this.progremUserService.doQueryById(NumberUtils.toInt(id));
        return success(result);
    }

    // 添加
    @ApiOperation(value = "新增加入保存", notes = "新增保存")
    @ResponseBody
    @RequestMapping(value = "join.htm")
    public ResultDTO join(@RequestBody ProgremUserDO progremUser) {
        Boolean result = false;
        UserBiz user = (UserBiz) getSessionUser();
        if (null == user) {
            return fail("请先登录");
        }
        progremUser.setUserId(user.getId());
        progremUser.setCreateTime(new Date());
        progremUser.setUserName(user.getNickName());
        //progremUser.setJoinTime(new Date());
        //状态1申请加入2申请通过3申请未通过0退出
        progremUser.setStatus(1);
        ProgremUserDO resultDO = this.progremUserService.doAdd(progremUser, super.getUserId());
        if (resultDO.getId() != null && resultDO.getId() != 0) {
            result = true;
        }
        return success(result);
    }
}