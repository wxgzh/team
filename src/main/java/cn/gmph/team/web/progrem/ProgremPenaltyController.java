package cn.gmph.team.web.progrem;

import java.util.Map;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import cn.gmph.team.service.progrem.ProgremPenaltyService;
import cn.gmph.team.service.progrem.entity.ProgremPenaltyDO;
import cn.gmph.common.base.ResultPageDTO;
import cn.gmph.common.base.ResultDTO;
import cn.gmph.common.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping(value ="/progremPenalty", produces = "text/html;charset=utf-8", method = RequestMethod.POST)
@Api(value = "/progremPenalty", tags = {"项目_惩罚"})
public class ProgremPenaltyController extends BaseController {

 @Autowired
 private ProgremPenaltyService progremPenaltyService;


 // 列表页
 @RequestMapping("/list.htm")
 public String list() {
     return "";
 }

 // 分页
 @ApiOperation(value = "分页查询", notes = "分页查询")
 @ResponseBody
 @RequestMapping(value = "query.htm", method = RequestMethod.GET)
 public ResultDTO page(@RequestBody ProgremPenaltyDO progremPenalty) {
     logger.info("开始分页查询ProgremPenaltyController.page, progremPenalty=" + progremPenalty.toString());
     Map<String, Integer> params = super.copyParamsToInteger(new String[] { "page", "rows" });
     Integer page = params.get("page");
     Integer rows = params.get("rows");
     ResultPageDTO<ProgremPenaltyDO> pager = this.progremPenaltyService.page(progremPenalty, page,rows);
     return success(pager);
 }

 // 添加
 @ApiOperation(value = "新增保存", notes = "新增保存")
 @ResponseBody
 @RequestMapping(value = "doAdd.htm")
 public ResultDTO doAdd (@RequestBody ProgremPenaltyDO progremPenalty) {
    ProgremPenaltyDO   resultDO = this.progremPenaltyService.doAdd(progremPenalty,super.getUserId());
    return success(resultDO);
 }

 // 修改
 @ApiOperation(value = "修改保存", notes = "修改保存")
 @ResponseBody
 @RequestMapping(value = "doUpdate.htm")
 public ResultDTO doUpdate (@RequestBody ProgremPenaltyDO progremPenalty) {
     Integer result = this.progremPenaltyService.doUpdate(progremPenalty,super.getUserId());
     return success(result);
 }

 // 删除
 @ApiOperation(value = "删除", notes = "删除")
 @ResponseBody
 @RequestMapping(value = "doDelete.htm")
 public ResultDTO doDelete(@RequestBody ProgremPenaltyDO progremPenalty) {
     Integer result = this.progremPenaltyService.doDelete(progremPenalty,super.getUserId());
     return success(result);
 }
 // 详情
 @ApiOperation(value = "查询详情", notes = "查询详情")
 @ResponseBody
 @RequestMapping(value = "detail.htm")
 public ResultDTO detail(@RequestParam String id) {
     ProgremPenaltyDO result = this.progremPenaltyService.doQueryById(NumberUtils.toInt(id));
     return success(result);
 }
}