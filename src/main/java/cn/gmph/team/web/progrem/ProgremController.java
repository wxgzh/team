package cn.gmph.team.web.progrem;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;

import cn.gmph.common.base.BaseController;
import cn.gmph.common.base.ResultDTO;
import cn.gmph.common.base.ResultPageDTO;
import cn.gmph.team.service.progrem.ProgremPenaltyService;
import cn.gmph.team.service.progrem.ProgremService;
import cn.gmph.team.service.progrem.ProgremSignService;
import cn.gmph.team.service.progrem.ProgremUserService;
import cn.gmph.team.service.progrem.entity.ProgremDO;
import cn.gmph.team.service.progrem.entity.ProgremPenaltyDO;
import cn.gmph.team.service.progrem.entity.ProgremSignDO;
import cn.gmph.team.service.progrem.entity.ProgremUserDO;
import cn.gmph.team.service.sys.UserBiz;
import cn.gmph.team.service.sys.UserService;
import cn.gmph.team.service.sys.entity.UserDO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping(value = "/progrem", produces = "text/html;charset=utf-8", method = RequestMethod.POST)
@Api(value = "/progrem", tags = { "项目" })
public class ProgremController extends BaseController {

    @Autowired
    private ProgremService        progremService;
    @Autowired
    private ProgremUserService    progremUserService;
    @Autowired
    private ProgremSignService    progremSignService;
    @Autowired
    private ProgremPenaltyService progremPenaltyService;
    @Autowired
    private UserService           userService;

    // 列表页
    @RequestMapping("/list.htm")
    public String list() {
        return "";
    }

    // 分页
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @ResponseBody
    @RequestMapping(value = "query.htm")
    public ResultDTO page(@RequestBody ProgremDO progrem) {
        logger.info("开始分页查询ProgremController.page, progrem=" + progrem.toString());
        Map<String, Integer> params = super.copyParamsToInteger(new String[] { "page", "rows" });
        Integer page = params.get("page");
        Integer rows = params.get("rows");
        ResultPageDTO<ProgremDO> pager = this.progremService.page(progrem, page, rows);
        return success(pager);
    }

    // 添加
    @ApiOperation(value = "新增保存", notes = "新增保存")
    @ResponseBody
    @RequestMapping(value = "doAdd.htm")
    public ResultDTO doAdd(@RequestBody ProgremDO progrem) {
        ProgremDO resultDO = this.progremService.doAdd(progrem, super.getUserId());
        return success(resultDO);
    }

    // 修改
    @ApiOperation(value = "修改保存", notes = "修改保存")
    @ResponseBody
    @RequestMapping(value = "doUpdate.htm")
    public ResultDTO doUpdate(@RequestBody ProgremDO progrem) {
        Integer result = this.progremService.doUpdate(progrem, super.getUserId());
        return success(result);
    }

    // 删除
    @ApiOperation(value = "删除", notes = "删除")
    @ResponseBody
    @RequestMapping(value = "doDelete.htm")
    public ResultDTO doDelete(@RequestBody ProgremDO progrem) {
        Integer result = this.progremService.doDelete(progrem, super.getUserId());
        return success(result);
    }

    // 详情
    @ApiOperation(value = "查询详情", notes = "查询详情")
    @ResponseBody
    @RequestMapping(value = "detail.htm")
    public ResultDTO detail(@RequestParam String id) {
        ProgremDO result = this.progremService.doQueryById(NumberUtils.toInt(id));
        UserBiz user = (UserBiz) getSessionUser();
        if (null == user) {
            result.setIsJoin(false);
            result.setIsPunishCard(true);
        } else {
            ProgremUserDO progremUserDO = progremUserService.doQueryByUserId(user.getId(), result.getId());
            if (null == progremUserDO) {
                result.setIsJoin(false);
            } else {
                result.setIsJoin(true);
            }
            if (result.getIsJoin() && 1 == result.getIsSignIn()) {
                List<ProgremSignDO> progremSignDOList = progremSignService.doQueryByUserId(user.getId(), result.getId());
                if (CollectionUtils.isEmpty(progremSignDOList)) {
                    result.setIsPunishCard(false);
                } else {
                    result.setIsPunishCard(true);
                }
            } else {
                result.setIsPunishCard(true);
                result.setIsJoin(true);
            }
        }
        return success(result);
    }

    // 加入人员投票查询
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @ResponseBody
    @RequestMapping(value = "joinUserList.htm")
    public ResultDTO joinUserList() {
        ResultPageDTO<ProgremUserDO> pager = new ResultPageDTO<ProgremUserDO>();
        List<ProgremUserDO> list = Lists.newArrayList();
        pager.setList(list);
        Map<String, Integer> params = super.copyParamsToInteger(new String[] { "page", "rows" });
        Integer page = params.get("page");
        Integer rows = params.get("rows");
        if (rows == 0) {
            rows = 20;
        }
        ProgremUserDO progremUser = new ProgremUserDO();
        UserBiz user = (UserBiz) getSessionUser();
        if (null == user) {
            return success(pager);
        }
        progremUser.setUserId(user.getId());
        pager = this.progremUserService.joinUserList(progremUser, page, rows);
        List<ProgremUserDO> userList = pager.getList();
        for (ProgremUserDO uDo : userList) {
            Integer progremId = uDo.getProgremId();
            Integer userId = uDo.getUserId();
            ProgremDO progrem = progremService.doQueryById(progremId);

            uDo.setProgremName(progrem.getName());
            UserDO userDO = userService.doQueryById(userId);
            uDo.setUserName(userDO.getNickName());
        }
        return success(pager);
    }

    //惩罚列表
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @ResponseBody
    @RequestMapping(value = "punishList.htm")
    public ResultDTO punishList() {
        Map<String, Integer> params = super.copyParamsToInteger(new String[] { "page", "rows" });
        Integer page = params.get("page");
        Integer rows = params.get("rows");
        if (rows == 0) {
            rows = 20;
        }
        ProgremPenaltyDO progremPenalty = new ProgremPenaltyDO();
        ResultPageDTO<ProgremPenaltyDO> pager = this.progremPenaltyService.punishList(progremPenalty, page, rows);
        List<ProgremPenaltyDO> userList = pager.getList();
        for (ProgremPenaltyDO uDo : userList) {
            Integer userId = uDo.getUserId();
            UserDO userDO = userService.doQueryById(userId);
            uDo.setUserName(userDO.getNickName());
        }
        return success(pager);
    }
}