package cn.gmph.team.web.progrem;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.gmph.common.base.BaseController;
import cn.gmph.common.base.ResultDTO;
import cn.gmph.common.base.ResultPageDTO;
import cn.gmph.team.service.progrem.ProgremSummService;
import cn.gmph.team.service.progrem.entity.ProgremSummDO;
import cn.gmph.team.service.sys.UserBiz;
import cn.gmph.team.service.sys.UserService;
import cn.gmph.team.service.sys.entity.UserDO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping(value = "/progremSumm", produces = "text/html;charset=utf-8", method = RequestMethod.POST)
@Api(value = "/progremSumm", tags = { "项目总结" })
public class ProgremSummController extends BaseController {

    @Autowired
    private ProgremSummService progremSummService;
    @Autowired
    private UserService        userService;

    // 列表页
    @RequestMapping("/list.htm")
    public String list() {
        return "";
    }

    // 分页
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @ResponseBody
    @RequestMapping(value = "query.htm", method = RequestMethod.GET)
    public ResultDTO page(@RequestBody ProgremSummDO progremSumm) {
        logger.info("开始分页查询ProgremSummController.page, progremSumm=" + progremSumm.toString());
        Map<String, Integer> params = super.copyParamsToInteger(new String[] { "page", "rows" });
        Integer page = params.get("page");
        Integer rows = params.get("rows");
        ResultPageDTO<ProgremSummDO> pager = this.progremSummService.page(progremSumm, page, rows);
        List<ProgremSummDO> resultList = pager.getList();
        for (ProgremSummDO sDo : resultList) {
            UserDO user = userService.doQueryById(sDo.getCreateId());
            if (null != user) {
                sDo.setName(user.getNickName());
            }
        }
        return success(pager);
    }

    // 添加
    @ApiOperation(value = "新增保存", notes = "新增保存")
    @ResponseBody
    @RequestMapping(value = { "doAdd.htm", "saveSummary.htm" })
    public ResultDTO doAdd(@RequestBody ProgremSummDO progremSumm) {
        Boolean result = true;
        progremSumm.setIsShow(1);
        progremSumm.setLastUpdateTime(new Date());
        UserBiz user = (UserBiz) getSessionUser();
        if (null == user) {
            return success(false);
        }
        progremSumm.setCreateId(user.getId());
        ProgremSummDO resultDO = this.progremSummService.doAdd(progremSumm, super.getUserId());
        return success(result);
    }

    // 修改
    @ApiOperation(value = "修改保存", notes = "修改保存")
    @ResponseBody
    @RequestMapping(value = "doUpdate.htm")
    public ResultDTO doUpdate(@RequestBody ProgremSummDO progremSumm) {
        Integer result = this.progremSummService.doUpdate(progremSumm, super.getUserId());
        return success(result);
    }

    // 删除
    @ApiOperation(value = "删除", notes = "删除")
    @ResponseBody
    @RequestMapping(value = "doDelete.htm")
    public ResultDTO doDelete(@RequestBody ProgremSummDO progremSumm) {
        Integer result = this.progremSummService.doDelete(progremSumm, super.getUserId());
        return success(result);
    }

    // 详情
    @ApiOperation(value = "查询详情", notes = "查询详情")
    @ResponseBody
    @RequestMapping(value = "detail.htm")
    public ResultDTO detail(@RequestParam String id) {
        ProgremSummDO result = this.progremSummService.doQueryById(NumberUtils.toInt(id));
        return success(result);
    }
}