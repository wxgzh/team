package cn.gmph.team.web.progrem;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;

import cn.gmph.common.base.BaseController;
import cn.gmph.common.base.ResultDTO;
import cn.gmph.common.base.ResultPageDTO;
import cn.gmph.team.service.progrem.ProgremSignService;
import cn.gmph.team.service.progrem.entity.ProgremSignDO;
import cn.gmph.team.service.sys.UserBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping(value = "/progremSign", produces = "text/html;charset=utf-8", method = RequestMethod.POST)
@Api(value = "/progremSign", tags = { "签到表" })
public class ProgremSignController extends BaseController {

    @Autowired
    private ProgremSignService progremSignService;

    // 列表页
    @RequestMapping("/list.htm")
    public String list() {
        return "";
    }

    // 分页
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @ResponseBody
    @RequestMapping(value = "query.htm")
    public ResultDTO page(@RequestBody ProgremSignDO progremSign) {
        logger.info("开始分页查询ProgremSignController.page, progremSign=" + progremSign.toString());
        Map<String, Integer> params = super.copyParamsToInteger(new String[] { "page", "rows" });
        Integer page = params.get("page");
        Integer rows = params.get("rows");
        ResultPageDTO<ProgremSignDO> pager = this.progremSignService.page(progremSign, page, rows);
        return success(pager);
    }

    // 添加
    @ApiOperation(value = "新增保存", notes = "新增保存")
    @ResponseBody
    @RequestMapping(value = "doAdd.htm")
    public ResultDTO doAdd(@RequestBody ProgremSignDO progremSign) {
        ProgremSignDO resultDO = this.progremSignService.doAdd(progremSign, super.getUserId());
        return success(resultDO);
    }

    // 修改
    @ApiOperation(value = "修改保存", notes = "修改保存")
    @ResponseBody
    @RequestMapping(value = "doUpdate.htm")
    public ResultDTO doUpdate(@RequestBody ProgremSignDO progremSign) {
        Integer result = this.progremSignService.doUpdate(progremSign, super.getUserId());
        return success(result);
    }

    // 删除
    @ApiOperation(value = "删除", notes = "删除")
    @ResponseBody
    @RequestMapping(value = "doDelete.htm")
    public ResultDTO doDelete(@RequestBody ProgremSignDO progremSign) {
        Integer result = this.progremSignService.doDelete(progremSign, super.getUserId());
        return success(result);
    }

    // 详情
    @ApiOperation(value = "查询详情", notes = "查询详情")
    @ResponseBody
    @RequestMapping(value = "detail.htm")
    public ResultDTO detail(@RequestParam String id) {
        ProgremSignDO result = this.progremSignService.doQueryById(NumberUtils.toInt(id));
        return success(result);
    }

    @ApiOperation(value = "打卡保存", notes = "新增保存")
    @ResponseBody
    @RequestMapping(value = "punchCard.htm")
    public ResultDTO punchCard(@RequestBody ProgremSignDO progremSign) {
        Boolean result = true;
        UserBiz user = (UserBiz) getSessionUser();
        if (null == user) {
            return success(false);
        }
        progremSign.setUserId(user.getId());
        progremSign.setSignTime(new Date());
        ProgremSignDO resultDO = this.progremSignService.doAdd(progremSign, super.getUserId());
        return success(result);
    }

    // 我的打卡记录分页
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @ResponseBody
    @RequestMapping(value = "mySignList.htm")
    public ResultDTO pageMySignList(@RequestBody ProgremSignDO progremSign) {
        logger.info("开始分页查询ProgremSignController.page, progremSign=" + progremSign.toString());
        Map<String, Integer> params = super.copyParamsToInteger(new String[] { "page", "rows" });
        Integer page = params.get("page");
        Integer rows = params.get("rows");
        if (rows == 0) {
            rows = 31;
        }
        ResultPageDTO<ProgremSignDO> pager = new ResultPageDTO<ProgremSignDO>();
        List<ProgremSignDO> list = Lists.newArrayList();
        pager.setList(list);
        UserBiz user = (UserBiz) getSessionUser();
        if (null == user) {
            return success(pager);
        }
        progremSign.setUserId(user.getId());
        try {
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat sf1 = new SimpleDateFormat("yyyy-MM");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.add(Calendar.MONTH, -2);
            String strStartTime = sf1.format(calendar.getTime());
            Date startTime = sf.parse(strStartTime + "-01 00:00:00");
            progremSign.setStartTime(startTime);
        } catch (Exception ex) {
            logger.error("日期转换报错" + ex);
        }
        pager = this.progremSignService.page(progremSign, page, rows);
        list = pager.getList();
        List<String> resultList = Lists.newArrayList();
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        for(ProgremSignDO sDo :list){
            String date = sf.format(sDo.getSignTime());
            resultList.add(date);
        }
        return success(resultList);
    }
}