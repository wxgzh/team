package cn.gmph.team.web.sys;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;

import cn.gmph.common.base.BaseController;
import cn.gmph.common.base.ResultDTO;
import cn.gmph.common.base.ResultPageDTO;
import cn.gmph.team.service.sys.UserBiz;
import cn.gmph.team.service.sys.VoteExtService;
import cn.gmph.team.service.sys.VoteService;
import cn.gmph.team.service.sys.entity.VoteDO;
import cn.gmph.team.service.sys.entity.VoteExtDO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping(value = "/vote", produces = "text/html;charset=utf-8", method = RequestMethod.POST)
@Api(value = "/vote", tags = { "系统_投票表" })
public class VoteController extends BaseController {

    @Autowired
    private VoteService    voteService;
    @Autowired
    private VoteExtService voteExtService;

    // 列表页
    @RequestMapping("/list.htm")
    public String list() {
        return "";
    }

    // 分页
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @ResponseBody
    @RequestMapping(value = { "query.htm", "voteList.htm" })
    public ResultDTO page(@RequestBody VoteDO vote) {
        logger.info("开始分页查询VoteController.page, vote=" + vote.toString());
        Map<String, Integer> params = super.copyParamsToInteger(new String[] { "page", "rows" });
        Integer page = params.get("page");
        Integer rows = params.get("rows");
        ResultPageDTO<VoteDO> pager = new ResultPageDTO<VoteDO>();
        List<VoteDO> list = Lists.newArrayList();
        pager.setList(list);
        UserBiz user = (UserBiz) getSessionUser();
        if (null == user) {
            return success(pager);
        }
        Integer voteUserId = user.getId();
        vote.setVoteUserId(voteUserId);
        pager = this.voteService.page(vote, page, rows);
        list = pager.getList();
        for (VoteDO tDo : list) {
            VoteExtDO voteExt = new VoteExtDO();
            voteExt.setVoteId(tDo.getId());
            ResultPageDTO<VoteExtDO> votePager = voteExtService.page(voteExt, 0, 10);
            List<VoteExtDO> voteExtList = votePager.getList();
            tDo.setVoteExtList(voteExtList);
        }
        return success(pager);
    }

    // 添加
    @ApiOperation(value = "新增保存", notes = "新增保存")
    @ResponseBody
    @RequestMapping(value = "doAdd.htm")
    public ResultDTO doAdd(@RequestBody VoteDO vote) {
        VoteDO resultDO = this.voteService.doAdd(vote, super.getUserId());
        return success(resultDO);
    }

    // 修改
    @ApiOperation(value = "修改保存", notes = "修改保存")
    @ResponseBody
    @RequestMapping(value = "doUpdate.htm")
    public ResultDTO doUpdate(@RequestBody VoteDO vote) {
        Integer result = this.voteService.doUpdate(vote, super.getUserId());
        return success(result);
    }

    // 删除
    @ApiOperation(value = "删除", notes = "删除")
    @ResponseBody
    @RequestMapping(value = "doDelete.htm")
    public ResultDTO doDelete(@RequestBody VoteDO vote) {
        Integer result = this.voteService.doDelete(vote, super.getUserId());
        return success(result);
    }

    // 详情
    @ApiOperation(value = "查询详情", notes = "查询详情")
    @ResponseBody
    @RequestMapping(value = "detail.htm")
    public ResultDTO detail(@RequestParam String id) {
        VoteDO result = this.voteService.doQueryById(NumberUtils.toInt(id));
        return success(result);
    }
}