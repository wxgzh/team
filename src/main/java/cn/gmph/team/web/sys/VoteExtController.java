package cn.gmph.team.web.sys;

import java.util.Map;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import cn.gmph.team.service.sys.VoteExtService;
import cn.gmph.team.service.sys.entity.VoteExtDO;
import cn.gmph.common.base.ResultPageDTO;
import cn.gmph.common.base.ResultDTO;
import cn.gmph.common.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping(value ="/voteExt", produces = "text/html;charset=utf-8", method = RequestMethod.POST)
@Api(value = "/voteExt", tags = {"系统_投票选项表"})
public class VoteExtController extends BaseController {

 @Autowired
 private VoteExtService voteExtService;


 // 列表页
 @RequestMapping("/list.htm")
 public String list() {
     return "";
 }

 // 分页
 @ApiOperation(value = "分页查询", notes = "分页查询")
 @ResponseBody
 @RequestMapping(value = "query.htm", method = RequestMethod.GET)
 public ResultDTO page(@RequestBody VoteExtDO voteExt) {
     logger.info("开始分页查询VoteExtController.page, voteExt=" + voteExt.toString());
     Map<String, Integer> params = super.copyParamsToInteger(new String[] { "page", "rows" });
     Integer page = params.get("page");
     Integer rows = params.get("rows");
     ResultPageDTO<VoteExtDO> pager = this.voteExtService.page(voteExt, page,rows);
     return success(pager);
 }

 // 添加
 @ApiOperation(value = "新增保存", notes = "新增保存")
 @ResponseBody
 @RequestMapping(value = "doAdd.htm")
 public ResultDTO doAdd (@RequestBody VoteExtDO voteExt) {
    VoteExtDO   resultDO = this.voteExtService.doAdd(voteExt,super.getUserId());
    return success(resultDO);
 }

 // 修改
 @ApiOperation(value = "修改保存", notes = "修改保存")
 @ResponseBody
 @RequestMapping(value = "doUpdate.htm")
 public ResultDTO doUpdate (@RequestBody VoteExtDO voteExt) {
     Integer result = this.voteExtService.doUpdate(voteExt,super.getUserId());
     return success(result);
 }

 // 删除
 @ApiOperation(value = "删除", notes = "删除")
 @ResponseBody
 @RequestMapping(value = "doDelete.htm")
 public ResultDTO doDelete(@RequestBody VoteExtDO voteExt) {
     Integer result = this.voteExtService.doDelete(voteExt,super.getUserId());
     return success(result);
 }
 // 详情
 @ApiOperation(value = "查询详情", notes = "查询详情")
 @ResponseBody
 @RequestMapping(value = "detail.htm")
 public ResultDTO detail(@RequestParam String id) {
     VoteExtDO result = this.voteExtService.doQueryById(NumberUtils.toInt(id));
     return success(result);
 }
}