package cn.gmph.team.web.sys;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.gmph.common.base.BaseController;
import cn.gmph.common.base.ResultDTO;
import cn.gmph.common.utils.DbUtil;
import cn.gmph.team.service.sys.UserBiz;
import cn.gmph.team.service.sys.UserService;
import cn.gmph.team.service.sys.entity.UserDO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping(value = "/comm", produces = "text/html;charset=utf-8", method = RequestMethod.POST)
@Api(value = "/comm", tags = { "公共方法" })
public class CommonController extends BaseController {

    @Autowired
    private UserService userService;

    @ApiOperation(value = "是否登录", notes = "查询详情")
    @ResponseBody
    @RequestMapping(value = "isLogin.htm")
    public ResultDTO isLogin() {
        Boolean result = true;
        UserBiz user = (UserBiz) getSessionUser();
        if (null == user) {
            result = false;
        }
        return success(result);
    }

    @ApiOperation(value = "登录用户信息查询", notes = "登录用户信息查询")
    @ResponseBody
    @RequestMapping(value = "userInfo.htm", method = RequestMethod.GET)
    public ResultDTO userInfo() {
        UserBiz user = (UserBiz) getSessionUser();
        if (null == user) {
            user = new UserBiz();
        }
        return success(user);
    }

    @ApiOperation(value = "是否登录", notes = "是否登录")
    @ResponseBody
    @RequestMapping(value = "login.htm")
    public ResultDTO login(@RequestBody UserDO argUser) {
        Boolean result = true;
        UserDO user = userService.doQueryByName(argUser.getName());
        if (null == user) {
            result = false;
            return success(false);
        }
        String password = DbUtil.MD5(argUser.getPassword());
        if (!password.equals(user.getPassword())) {
            return success(false);
        }
        UserBiz userBiz = new UserBiz();
        userBiz.setName(user.getName());
        userBiz.setNickName(user.getNickName());
        userBiz.setMobilel(user.getMobilel());
        userBiz.setId(user.getId());
        setSessionUser(userBiz);
        return success(result);
    }

    @ApiOperation(value = "注册", notes = "注册")
    @ResponseBody
    @RequestMapping(value = "register.htm")
    public ResultDTO register(@RequestBody UserDO argUser) {
        Boolean result = true;
        UserDO user = userService.doQueryByName(argUser.getName());
        if (null != user) {
            result = false;
            return fail(1000, "用户已存在");
        }
        String password = DbUtil.MD5(argUser.getPassword());
        argUser.setPassword(password);
        argUser.setMobilel(argUser.getName());
        userService.doAdd(argUser, super.getUserId());
        return success(result);
    }
    
    @ApiOperation(value = "修改密码", notes = "修改密码")
    @ResponseBody
    @RequestMapping(value = "resetPassword.htm")
    public ResultDTO resetPassword(@RequestBody UserDO argUser) {
        Boolean result = true;
        UserDO user = userService.doQueryByName(argUser.getName());
        if (null == user) {
            result = false;
            return fail("用户不存在");
        }
        String oldPassword = DbUtil.MD5(argUser.getOldPassword());
        if (!oldPassword.equals(user.getPassword())) {
            return fail("原密码错误");
        }
        String password = DbUtil.MD5(argUser.getPassword());
        argUser.setPassword(password);
        userService.doUpdate(argUser, super.getUserId());
        return success(result);
    }
    
    @ApiOperation(value = "忘记密码", notes = "修改密码")
    @ResponseBody
    @RequestMapping(value = "forgetPassword.htm")
    public ResultDTO forgetPassword(@RequestBody UserDO argUser) {
        Boolean result = true;
        UserDO user = userService.doQueryByName(argUser.getName());
        if (null == user) {
            result = false;
            return fail("用户不存在");
        }
        int newPassword = (int)((Math.random()*9+1)*100000);  
        argUser.setPassword(String.valueOf(newPassword));
        userService.doUpdate(argUser, super.getUserId());
        //发送短信至手机号码
        return success(result);
    }
}