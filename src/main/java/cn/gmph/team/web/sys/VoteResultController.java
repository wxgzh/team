package cn.gmph.team.web.sys;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.gmph.common.base.BaseController;
import cn.gmph.common.base.ResultDTO;
import cn.gmph.common.base.ResultPageDTO;
import cn.gmph.team.service.sys.UserBiz;
import cn.gmph.team.service.sys.VoteResultService;
import cn.gmph.team.service.sys.entity.VoteResultDO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping(value = "/voteResult", produces = "text/html;charset=utf-8", method = RequestMethod.POST)
@Api(value = "/voteResult", tags = { "系统_投票_结果表" })
public class VoteResultController extends BaseController {

    @Autowired
    private VoteResultService voteResultService;

    // 列表页
    @RequestMapping("/list.htm")
    public String list() {
        return "";
    }

    // 分页
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @ResponseBody
    @RequestMapping(value = "query.htm", method = RequestMethod.GET)
    public ResultDTO page(@RequestBody VoteResultDO voteResult) {
        logger.info("开始分页查询VoteResultController.page, voteResult=" + voteResult.toString());
        Map<String, Integer> params = super.copyParamsToInteger(new String[] { "page", "rows" });
        Integer page = params.get("page");
        Integer rows = params.get("rows");
        ResultPageDTO<VoteResultDO> pager = this.voteResultService.page(voteResult, page, rows);
        return success(pager);
    }

    // 添加
    @ApiOperation(value = "新增保存", notes = "新增保存")
    @ResponseBody
    @RequestMapping(value = { "doAdd.htm", "voteSave.htm" })
    public ResultDTO doAdd(@RequestBody VoteResultDO voteResult) {
        Boolean result = true;
        UserBiz user = (UserBiz) getSessionUser();
        if (null == user) {
            return success(false);
        }
        voteResult.setVoteTime(new Date());
        voteResult.setUserId(user.getId());
        VoteResultDO resultDO = this.voteResultService.doAdd(voteResult, super.getUserId());
        //查询项目，获取项目人，查询已投票的信息，占半就完成。
        
        return success(result);
    }

    // 修改
    @ApiOperation(value = "修改保存", notes = "修改保存")
    @ResponseBody
    @RequestMapping(value = "doUpdate.htm")
    public ResultDTO doUpdate(@RequestBody VoteResultDO voteResult) {
        Integer result = this.voteResultService.doUpdate(voteResult, super.getUserId());
        return success(result);
    }

    // 删除
    @ApiOperation(value = "删除", notes = "删除")
    @ResponseBody
    @RequestMapping(value = "doDelete.htm")
    public ResultDTO doDelete(@RequestBody VoteResultDO voteResult) {
        Integer result = this.voteResultService.doDelete(voteResult, super.getUserId());
        return success(result);
    }

    // 详情
    @ApiOperation(value = "查询详情", notes = "查询详情")
    @ResponseBody
    @RequestMapping(value = "detail.htm")
    public ResultDTO detail(@RequestParam String id) {
        VoteResultDO result = this.voteResultService.doQueryById(NumberUtils.toInt(id));
        return success(result);
    }
}