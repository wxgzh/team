package cn.gmph.framework.aop.annotation.aspect;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import cn.gmph.framework.aop.annotation.Log4j;

@Component
@Aspect
@Order(9)
public class Log4jAspect {

	private Logger logger = Logger.getLogger(this.getClass());

	@Around(value = "@annotation(log4j)", argNames = "log4j")
	public Object Around(ProceedingJoinPoint joinPoint, Log4j log4j) throws Throwable {
		logger.warn("");
		logger.warn("");
		this.printArgs(joinPoint);
		Object obj = joinPoint.proceed();
		logger.warn("返回结果[" + obj + "]");

		return obj;
	}

	private void printArgs(ProceedingJoinPoint joinPoint) {
		try {
			MethodSignature signature = (MethodSignature) joinPoint.getSignature();
			if (signature == null) {
				return;
			}
			logger.warn("打印参数和返回值[" + StringUtils.remove(signature.getDeclaringTypeName(),"cn.zuixiandao.llz.") + "." + signature.getName()+"]");
			String[] argsKeyArr = signature.getParameterNames();
			if (argsKeyArr == null || argsKeyArr.length == 0) {
				return;
			}
			Object[] argsValueArr = joinPoint.getArgs();
			for (int i = 0; i < argsKeyArr.length; i++) {
				logger.warn("参数["+argsKeyArr[i] + "],[" + argsValueArr[i]+"]");
			}
		} catch (Exception e) {
			logger.error("CacheAspect getValue Exception", e);
		}
	}
}
