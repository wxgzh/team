package cn.gmph.framework.aop.annotation.aspect;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import cn.gmph.framework.aop.annotation.CheckLogin;

@Component
@Aspect
@Order(2)
public class CheckLoginAspect {

    private Logger logger = Logger.getLogger(this.getClass());

    @Around(value = "@annotation(checkLogin)", argNames = "checkLogin")
    public Object Around(ProceedingJoinPoint joinPoint, CheckLogin checkLogin) throws Throwable {
        return joinPoint.proceed();
    }

}
