package cn.gmph.framework.aop.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RequestValidate {

	// 需要参与合法性的客户端参数
	String[] names() default {};

	// 来自客户端的密文
	String ciphertext() default "";
	//是否进行唯一性登陆验证
	boolean ifCheck() default false;
}
