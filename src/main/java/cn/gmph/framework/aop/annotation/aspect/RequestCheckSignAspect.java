package cn.gmph.framework.aop.annotation.aspect;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import cn.gmph.framework.aop.annotation.RequestChecksign;

@Aspect
@Order(2)
@Component
public class RequestCheckSignAspect {
    private static Logger logger = Logger.getLogger(RequestCheckSignAspect.class);

    @Around(value = "@annotation(rv)", argNames = "rv")
    public Object around(ProceedingJoinPoint jp, RequestChecksign rv) throws Throwable {
        return jp.proceed();
    }

}
