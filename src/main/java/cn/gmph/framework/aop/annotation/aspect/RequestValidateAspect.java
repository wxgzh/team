package cn.gmph.framework.aop.annotation.aspect;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import cn.gmph.framework.aop.annotation.RequestValidate;

@Aspect
@Order(1)
@Component
public class RequestValidateAspect {

    private Logger logger = Logger.getLogger(this.getClass());

    @Around(value = "@annotation(rv)", argNames = "rv")
    public Object around(ProceedingJoinPoint jp, RequestValidate rv) throws Throwable {
        return jp.proceed();
    }

}
