package cn.gmph.framework.aop;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * 读取配置文件中的host变量添加到view中,页面上可以直接调用
 * 
 * @author yongfei.zheng
 * @date 2015年10月18日上午10:37:30
 */
@Component
public class PropertiesInterceptor implements HandlerInterceptor {

	@Autowired
	private Environment env;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		try {
			if (modelAndView != null) {
				modelAndView.addObject("host", this.env.getProperty("project.host"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

	}

}
