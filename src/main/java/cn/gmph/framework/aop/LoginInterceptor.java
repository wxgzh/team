package cn.gmph.framework.aop;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * 检查登录情况
 * 
 * @author Administrator
 *
 */
@Component
public class LoginInterceptor implements HandlerInterceptor {

	@Autowired
	private Environment env;

	//@Autowired
	//private LoginService loginService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		// 从配置文件中获取浙付通接口模块,不需要被拦截
		String appModules = this.env.getProperty("app.modules");
		if (StringUtils.isNotBlank(appModules)) {
			String[] modules = StringUtils.split(appModules, ",");
			for (String module : modules) {
				if (StringUtils.contains(request.getRequestURL().toString(), module)) {
					return true;
				}
			}
		}
		if (StringUtils.contains(request.getRequestURL().toString(), "login.htm")) {
			return true;
		}
		if (StringUtils.contains(request.getRequestURL().toString(), "doLogin.htm")) {
			return true;
		}
		if (StringUtils.contains(request.getRequestURL().toString(), "forgetPwd.htm")) {
			return true;
		}
		if (StringUtils.contains(request.getRequestURL().toString(), "findPwdAndLogin.htm")) {
			return true;
		}
		request.getSession();
		/*Result<Integer> result = this.loginService.getUserIdFromCache(request);
		if (!result.isSuccess()) {
			response.sendRedirect(this.env.getProperty("fns.posp-admin.host") + "/login/login.htm");
			return true;
		}*/
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

	}
}
