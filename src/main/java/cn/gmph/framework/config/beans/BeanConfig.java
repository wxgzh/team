package cn.gmph.framework.config.beans;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.http.MediaType;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.google.common.collect.Lists;

@Configuration
@ComponentScan(basePackages = { "cn.gmph.framework.aop", "cn.gmph.team.service", "cn.gmph.common" })
@EnableAspectJAutoProxy
public class BeanConfig extends WebMvcConfigurationSupport {

    // 文件上传
    @Bean(name = "multipartResolver")
    public MultipartResolver getMultipartResolver() {
        CommonsMultipartResolver m = new CommonsMultipartResolver();
        m.setMaxUploadSize(10485760l);
        m.setResolveLazily(true);
        return m;
    }

    @Bean
    public RequestMappingHandlerMapping requestMappingHandlerMapping() {
        RequestMappingHandlerMapping a = new RequestMappingHandlerMapping();
        return super.requestMappingHandlerMapping();
    }

    @Bean
    public RequestMappingHandlerAdapter requestMappingHandlerAdapter() {
        RequestMappingHandlerAdapter adapter = new RequestMappingHandlerAdapter();
        List converterList = Lists.newArrayList();
        FastJsonHttpMessageConverter fastJsonConverter = new FastJsonHttpMessageConverter();
        //FastJsonConfig fastJsonConfig = new FastJsonConfig();
        //SerializeConfig serializeConfig = new SerializeConfig();

        //fastJsonConfig.setSerializeConfig(serializeConfig);
        //fastJsonConverter.setFastJsonConfig(fastJsonConfig);
        fastJsonConverter.setFeatures(SerializerFeature.WriteMapNullValue,
            SerializerFeature.WriteNullStringAsEmpty);

        converterList.add(fastJsonConverter);
        converterList.add(new ByteArrayHttpMessageConverter());
        StringHttpMessageConverter stringConverter = new StringHttpMessageConverter();
        List<MediaType> supportedMediaTypes = Lists.newArrayList();
        MediaType type = MediaType.parseMediaType("text/html;charset=UTF-8");
        supportedMediaTypes.add(type);
        stringConverter.setSupportedMediaTypes(supportedMediaTypes);
        converterList.add(stringConverter);
        adapter.setMessageConverters(converterList);
        return adapter;
    }
}
