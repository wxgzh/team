package cn.gmph.framework.config.beans;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@MapperScan(basePackages = "cn.gmph.oracle.dao", sqlSessionFactoryRef = "oracleSqlSessionFactory")
@EnableTransactionManagement
public class MybatisOracle {
}
