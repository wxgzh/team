package cn.gmph.framework.config.beans;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@ComponentScan(basePackages = { "cn.gmph.team.timetask" })
@EnableScheduling
public class TimetaskConfig {
}
