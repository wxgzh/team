package cn.gmph.framework.config;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.apache.log4j.Logger;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;

import cn.gmph.framework.config.beans.BeanConfig;
import cn.gmph.framework.config.beans.ExceptionConfig;
import cn.gmph.framework.config.beans.MybatisConfig;
import cn.gmph.framework.config.beans.MybatisOracle;
import cn.gmph.framework.config.beans.MysqlDataSource;
import cn.gmph.framework.config.beans.PropertiesConfig;
import cn.gmph.framework.config.beans.TimetaskConfig;
import cn.gmph.framework.config.mvc.MvcConfig;
import cn.gmph.framework.config.springext.MyContextLoaderListener;
import cn.gmph.framework.log.filter.WebAccessLogFilter;

public class Config implements WebApplicationInitializer {

    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    @SuppressWarnings("rawtypes")
    public void onStartup(ServletContext servletContext) throws ServletException {
        logger.info("init WebApplicationInitializer");

        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();

        rootContext.register(PropertiesConfig.class);// 装载xml配置和properties
        rootContext.register(MysqlDataSource.class);// jdbc
 
        //		servletContext.addListener(new ContextLoaderListener(rootContext));
        servletContext.addListener(new MyContextLoaderListener(rootContext));

        AnnotationConfigWebApplicationContext dispatcherServlet = new AnnotationConfigWebApplicationContext();
        dispatcherServlet.register(MvcConfig.class);
        dispatcherServlet.register(BeanConfig.class);
        dispatcherServlet.register(TimetaskConfig.class);
        dispatcherServlet.register(MybatisConfig.class);
        dispatcherServlet.register(MybatisOracle.class);
        dispatcherServlet.register(ExceptionConfig.class);
        ServletRegistration.Dynamic dispatcher = servletContext.addServlet("dispatcher", new DispatcherServlet(dispatcherServlet));
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("*.htm","/configuration/ui","/swagger-resources","/v2/api-docs","/configuration/security");
        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding("UTF-8");
        characterEncodingFilter.setForceEncoding(true);
        FilterRegistration.Dynamic filter = servletContext.addFilter("characterEncodingFileter", characterEncodingFilter);
        filter.addMappingForUrlPatterns(null, true, "*.htm");
        //web访问日志处理
        WebAccessLogFilter webAccessLogFilter = new WebAccessLogFilter();
        FilterRegistration.Dynamic filter2 = servletContext.addFilter("webAccessLogFilter", webAccessLogFilter);
        filter2.addMappingForUrlPatterns(null, true, "*.htm");
    }

}