package cn.gmph.framework.config.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import cn.gmph.framework.aop.LoginInterceptor;
import cn.gmph.framework.aop.PropertiesInterceptor;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = { "cn.gmph.team.web", "cn.gmph","cn.gmph.framework.swagger" })
@EnableAspectJAutoProxy
public class MvcConfig extends WebMvcConfigurerAdapter {

    @Autowired
    private PropertiesInterceptor propertiesInterceptor;

    @Autowired
    private LoginInterceptor      loginInterceptor;

    @Bean
    public InternalResourceViewResolver jspViewResolver() {
        InternalResourceViewResolver bean = new InternalResourceViewResolver();
        bean.setPrefix("/template/");
        bean.setSuffix(".jsp");
        return bean;
    }

    // 注册拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(propertiesInterceptor);
        registry.addInterceptor(loginInterceptor);
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
    }

    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
    }
}
