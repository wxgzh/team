package cn.gmph.framework.config.springext;

import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.web.context.ConfigurableWebApplicationContext;
import org.springframework.web.context.WebApplicationContext;

public class MyContextLoaderListener extends org.springframework.web.context.ContextLoaderListener {

    public MyContextLoaderListener(WebApplicationContext context) {
        super(context);
    }

    @Override
    protected void customizeContext(ServletContext sc, ConfigurableWebApplicationContext wac) {
        Resource resource = new ClassPathResource("config.properties");
        Properties properties = new Properties();
        try {
            properties.load(resource.getInputStream());
        } catch (IOException e) {
            throw new RuntimeException("加载配置文件config.properties失败", e);
        }
        String profile = properties.getProperty("profile");
        if (StringUtils.isBlank(profile)) {
            throw new RuntimeException("没有配置profile,spring启动失败");
        }
        wac.getEnvironment().setActiveProfiles(profile);
        super.customizeContext(sc, wac);
    }
}
